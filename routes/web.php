<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'InicioController@index')->name('inicio');



Route::get('seguridad/login', 'Seguridad\LoginController@index')->name('login');
Route::post('seguridad/login', 'Seguridad\LoginController@login')->name('login_post');
Route::get('seguridad/logout', 'Seguridad\LoginController@logout')->name('logout');
Route::post('ajax-sesion', 'AjaxController@setSession')->name('ajax')->middleware('auth');

// Ver lo de superadmin en middleware
Route::group(['prefix'=>'admin','namespace'=>'Admin','middleware'=>['auth'] ],function(){
    Route::get('','AdminController@index');

    

    // MENU
    Route::get('menu', 'MenuController@index')->name('menu');
    Route::get('menu/crear', 'MenuController@create')->name('crear_menu');
    Route::post('menu', 'MenuController@store')->name('guardar_menu');
    Route::get('menu/{id}/editar', 'MenuController@edit')->name('editar_menu');
    Route::put('menu/{id}', 'MenuController@update')->name('actualizar_menu');
    Route::get('menu/{id}/eliminar', 'MenuController@delete')->name('eliminar_menu');
    Route::post('menu/guardar-orden', 'MenuController@guardarOrden')->name('guardar_orden');
    // ROL
    Route::get('rol', 'RolController@index')->name('rol');
    Route::get('rol/crear', 'RolController@create')->name('crear_rol');
    Route::post('rol', 'RolController@store')->name('guardar_rol');
    Route::get('rol/{id}/editar', 'RolController@edit')->name('editar_rol');
    Route::put('rol/{id}', 'RolController@update')->name('actualizar_rol');
    Route::delete('rol/{id}', 'RolController@delete')->name('eliminar_rol');
    // MENU-ROL
    Route::get('menu-rol', 'MenuRolController@index')->name('menu_rol');
    Route::post('menu-rol', 'MenuRolController@store')->name('guardar_menu_rol');

    // PERMISO
    Route::get('permiso', 'PermisoController@index')->name('permiso');
    Route::get('permiso/crear', 'PermisoController@create')->name('crear_permiso');
    Route::post('permiso', 'PermisoController@store')->name('guardar_permiso');
    Route::get('permiso/{id}/editar', 'PermisoController@edit')->name('editar_permiso');
    Route::put('permiso/{id}', 'PermisoController@update')->name('actualizar_permiso');
    Route::delete('permiso/{id}', 'PermisoController@delete')->name('eliminar_permiso');

    /*RUTAS MENU_ROL*/
    Route::get('menu-rol', 'MenuRolController@index')->name('menu_rol');
    Route::post('menu-rol', 'MenuRolController@store')->name('guardar_menu_rol');
    /*RUTAS PERMISO_ROL*/
    Route::get('permiso-rol', 'PermisoRolController@index')->name('permiso_rol');
    Route::post('permiso-rol', 'PermisoRolController@store')->name('guardar_permiso_rol');

    /*RUTAS DE USUARIO*/
    Route::get('usuario', 'UsuarioController@index')->name('usuario');
    Route::get('usuario/crear', 'UsuarioController@create')->name('crear_usuario');
    Route::post('usuario', 'UsuarioController@store')->name('guardar_usuario');
    Route::get('usuario/{id}/editar', 'UsuarioController@edit')->name('editar_usuario');
    Route::put('usuario/{id}', 'UsuarioController@update')->name('actualizar_usuario');
    Route::delete('usuario/{id}', 'UsuarioController@delete')->name('eliminar_usuario');

    /* ESPECIALIDADES */
    Route::get('especialidad', 'EspecialidadController@index')->name('especialidad');
    Route::get('especialidad/crear', 'EspecialidadController@create')->name('crear_especialidad');
    Route::post('especialidad', 'EspecialidadController@store')->name('guardar_especialidad');
    Route::get('especialidad/{id}/editar', 'EspecialidadController@edit')->name('editar_especialidad');
    Route::put('especialidad/{id}', 'EspecialidadController@update')->name('actualizar_especialidad');
    Route::delete('especialidad/{id}', 'EspecialidadController@delete')->name('eliminar_especialidad');

     /* SERVICIOS */
     Route::get('servicio', 'ServicioController@index')->name('servicio');
     Route::get('servicio/crear', 'ServicioController@create')->name('crear_servicio');
     Route::post('servicio', 'ServicioController@store')->name('guardar_servicio');
     Route::get('servicio/{id}/editar', 'ServicioController@edit')->name('editar_servicio');
     Route::put('servicio/{id}', 'ServicioController@update')->name('actualizar_servicio');
     Route::delete('servicio/{id}', 'ServicioController@delete')->name('eliminar_servicio');

     Route::get('odontologo', 'OdontologoController@index')->name('odontologo');
     Route::get('odontologo/crear', 'OdontologoController@create')->name('crear_odontologo');
     Route::post('odontologo', 'OdontologoController@store')->name('guardar_odontologo');
     Route::get('odontologo/{ci}/editar', 'OdontologoController@edit')->name('editar_odontologo');
     Route::put('odontologo/{ci}', 'OdontologoController@update')->name('actualizar_odontologo');
     Route::delete('odontologo/{ci}', 'OdontologoController@delete')->name('eliminar_odontologo');

     Route::get('paciente', 'PacienteController@index')->name('paciente');
     Route::get('paciente/crear', 'PacienteController@create')->name('crear_paciente');
     Route::post('paciente', 'PacienteController@store')->name('guardar_paciente');
     Route::get('paciente/{ci}/editar', 'PacienteController@edit')->name('editar_paciente');
     Route::put('paciente/{ci}', 'PacienteController@update')->name('actualizar_paciente');
     Route::delete('paciente/{ci}', 'PacienteController@delete')->name('eliminar_paciente');

     Route::get('recepcionista', 'RecepcionistaController@index')->name('recepcionista');
     Route::get('recepcionista/crear', 'RecepcionistaController@create')->name('crear_recepcionista');
     Route::post('recepcionista', 'RecepcionistaController@store')->name('guardar_recepcionista');
     Route::get('recepcionista/{ci}/editar', 'RecepcionistaController@edit')->name('editar_recepcionista');
     Route::put('recepcionista/{ci}', 'RecepcionistaController@update')->name('actualizar_recepcionista');
     Route::delete('recepcionista/{ci}', 'RecepcionistaController@delete')->name('eliminar_recepcionista');

     /*  CITAS */
     Route::resource('/cita' ,'CitaController');
    /**Bitacora */
    Route::get(   '/bitacora',             'BitacoraController@index')->name('bitacora_index');
     /** CONSULTAS  */
     Route::get(   'consulta',             'ConsultaController@index')->name('consulta_index');
     Route::get(   'consulta/crear',       'ConsultaController@create')->name('crear_consulta');
     Route::post(  'consulta',             'ConsultaController@store')->name('guardar_consulta');
     Route::get(   'consulta/{id}/editar', 'ConsultaController@edit')->name('editar_consulta');
     Route::put(   'consulta/{id}',        'ConsultaController@update')->name('actualizar_consulta');
     Route::delete('consulta/{id}',        'ConsultaController@delete')->name('eliminar_consulta');
     Route::get('consulta/verCitas',        'ConsultaController@verCitas')->name('verCitasConsulta');
     
     /*Materia Prima*/
     Route::get('materia-prima', 'MateriaPrimaController@index')->name('materiaprima');
     Route::get('materia-prima/crear', 'MateriaPrimaController@create')->name('crear-materiaprima');
     Route::post('materia-prima','MateriaPrimaController@store')->name('guardar_materiaprima');
     Route::get('materia-prima/{id}/editar', 'MateriaPrimaController@edit')->name('editar_materiaprima');
     Route::put('materia-prima/{id}', 'MateriaPrimaController@update')->name('actualizar_materiaprima');
     Route::delete('materia-prima/{id}', 'MateriaPrimaController@delete')->name('eliminar_materiaprima');
    //Nota Inventario
    Route::get('nota-inventario', 'NotaInventarioController@index')->name('notainventario');
    Route::get('nota-inventario/crear', 'NotaInventarioController@create')->name('crear_notainventario');
    Route::post('nota-inventario', 'NotaInventarioController@store')->name('guardar_notainventario');
    Route::get('nota-inventario/{id}/editar', 'NotaInventarioController@edit')->name('editar_notainventario');
    Route::put('nota-inventario/{id}', 'NotaInventarioController@update')->name('actualizar_notainventario');
    Route::delete('nota-inventario/{id}', 'NotaInventarioController@delete')->name('eliminar_notainventario');

     Route::get(   'estudiosComplementarios',             'EstudioComplementarioController@index')->name('estudio');
     Route::get(   'estudiosComplementarios/crear',       'EstudioComplementarioController@create')->name('crear_estudio');
     Route::post(  'estudiosComplementarios',             'EstudioComplementarioController@store')->name('guardar_estudio');
     Route::get(   'estudiosComplementarios/{ci}/editar', 'EstudioComplementarioController@edit')->name('editar_estudio');
     Route::put(   'estudiosComplementarios/{ci}',        'EstudioComplementarioController@update')->name('actualizar_estudio');
     Route::delete('estudiosComplementarios/{ci}',        'EstudioComplementarioController@delete')->name('eliminar_estudio');

     Route::get(   'horarios',             'HorarioController@index')->name('horarios');
     Route::get(   'horarios/crear',       'HorarioController@create')->name('crear_horarios');
     Route::post(  'horarios',             'HorarioController@store')->name('guardar_horarios');
     Route::get(   'horarios/{ci}/editar', 'HorarioController@edit')->name('editar_horarios');
     Route::put(   'horarios/{ci}',        'HorarioController@update')->name('actualizar_horarios');
     Route::delete('horarios/{ci}',        'HorarioController@delete')->name('eliminar_horarios');

     Route::get(   'receta',             'RecetaController@index')->name('receta');
     Route::get(   'receta/crear',       'RecetaController@create')->name('crear_receta');
     Route::post(  'receta',             'RecetaController@store')->name('guardar_receta');
     Route::get(   'receta/{ci}/editar', 'RecetaController@edit')->name('editar_receta');
     Route::put(   'receta/{ci}',        'RecetaController@update')->name('actualizar_receta');
     Route::delete('receta/{ci}',        'RecetaController@delete')->name('eliminar_receta');

     Route::get(   'odontogramaTest',             'OdontogramaController@index')->name('odontograma');
     Route::post(  'odontogramaTest',             'OdontogramaController@store')->name('guardar_odontograma');

     Route::get(   'listado_citas',             'CitaController@show')->name('listado_citas');
     Route::get(   'receta/{ci}/mostrar', 'CitaController@edit')->name('editar_mostrar');
     
     Route::get('notaVenta', 'NotaVentaController@index')->name('notaVenta');

     Route::get('cuota', 'CuotaController@index')->name('cuota');

    //historialMedico
    
    Route::get(   'historial',             'HistorialController@index')->name('historial');
    Route::post(  'historial',             'HistorialController@store')->name('guardar_historial');
    Route::get(   'historial/{ci}/editar', 'HistorialController@edit')->name('editar_historial');
    Route::put(   'historial/{ci}',        'HistorialController@update')->name('actualizar_historial');
    Route::delete('historial/{ci}',        'HistorialController@delete')->name('eliminar_historial');

}
);