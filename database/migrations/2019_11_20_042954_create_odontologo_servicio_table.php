<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOdontologoServicioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('odontologo_servicio', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('odontologo_ci');
            $table->foreign('odontologo_ci','fk_odontologoservicio_odontologo')->references('ci')->on('odontologo')->onDelete('cascade')->onUpdate('cascade');
            
            $table->unsignedInteger('servicio_id');
            $table->foreign('servicio_id','fk_odontologoservicio_servicio')->references('id')->on('servicio')->onDelete('cascade')->onUpdate('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('odontologo_servicio');
    }
}
