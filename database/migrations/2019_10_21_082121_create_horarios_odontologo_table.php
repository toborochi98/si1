<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorariosOdontologoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horarios_odontologo', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('horarios_id');
            $table->foreign('horarios_id','fk_horariosodontologo_horarios')->references('id')->on('horarios')->onDelete('cascade')->onUpdate('cascade');
            
            $table->unsignedInteger('odontologo_ci');
            $table->foreign('odontologo_ci','fk_horariosodontologo_odontologo')->references('ci')->on('odontologo')->onDelete('cascade')->onUpdate('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horarios_odontologo');
    }
}
