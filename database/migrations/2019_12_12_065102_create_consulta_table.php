<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consulta', function (Blueprint $table) {
            $table->increments('id');
            $table->string('motivo',256);
            $table->string('diagnostico',256);
            $table->string('tratamiento',256);
            $table->date('fecha_retorno');
            $table->unsignedInteger('historial_id');
            $table->foreign('historial_id')->references('id')->on('historial')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedInteger('cita_id');
            $table->foreign('cita_id')->references('id')->on('citas')->onDelete('cascade')->onUpdate('cascade');
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consulta');
    }
}
