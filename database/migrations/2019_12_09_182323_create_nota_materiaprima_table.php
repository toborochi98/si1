<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotaMateriaprimaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nota_materiaprima', function (Blueprint $table) {
            $table->unsignedInteger('notainventario_id');
            $table->foreign('notainventario_id','fk_notamateriaprima_notainventario')->references('id')->on('notainventario')->onDelete('cascade')->onUpdate('restrict');
            
            $table->unsignedInteger('materiaprimas_id');
            $table->foreign('materiaprimas_id','fk_notamateriaprima_materiaprimas')->references('id')->on('materiaprima')->onDelete('restrict')->onUpdate('restrict')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nota_materiaprima');
    }
}
