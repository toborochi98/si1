<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOdontologoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('odontologo', function (Blueprint $table) {
            $table->unsignedInteger('ci');
            $table->primary('ci');
            $table->string('nombre',64);
            $table->string('apellido_paterno',64);
            $table->string('apellido_materno',64);
            $table->date('fecha_nacimiento');
            $table->string('domicilio',128);
            $table->string('correo',64)->nullable();
            $table->string('telefono',16)->nullable();;
            $table->unsignedInteger('usuario_id');
            $table->foreign('usuario_id')->references('id')->on('usuario')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('odontologo');
    }
}
