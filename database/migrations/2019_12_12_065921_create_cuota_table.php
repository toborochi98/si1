<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuotaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuota', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('paciente_id');
            $table->foreign('paciente_id')->references('ci')->on('paciente')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedInteger('notaVenta_id');
            $table->foreign('notaVenta_id')->references('id')->on('notaventa')->onDelete('cascade')->onUpdate('cascade');
            $table->float('monto');
            $table->boolean('estado');
            $table->date('fecha');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receta');
    }
}
