<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEspecialidadOdontologoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('especialidad_odontologo', function (Blueprint $table) {
            $table->unsignedInteger('especialidad_id');
            $table->foreign('especialidad_id', 'fk_especialidadodontologo_especialidad')->references('id')->on('especialidad')->onDelete('restrict')->onUpdate('restrict');
            $table->unsignedInteger('odontologo_ci');
            $table->foreign('odontologo_ci', 'fk_especialidadodontologo_odontologo')->references('ci')->on('odontologo')->onDelete('cascade')->onUpdate('restrict');    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('especialidad_odontologo');
    }
}
