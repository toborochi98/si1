<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citas', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('estado');
            $table->date('fecha'); 
            
            $table->time('hora_inicio');
            $table->time('hora_fin');
            $table->unsignedInteger('odonto_id');
            $table->foreign('odonto_id')->references('ci')->on('odontologo');
            $table->unsignedInteger('paciente_id');
            
            $table->foreign('paciente_id')->references('ci')->on('paciente');
            
            $table->timestamps();
            $table->softDeletes();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citas');
    }
}
