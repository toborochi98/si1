<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCpDentalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cp_dental', function (Blueprint $table) {
            $table->increments('id');
            $table->string('estado_diagnostico',64);
            $table->string('estado_tratamiento',64);
            $table->unsignedInteger('diente_id');
            $table->foreign('diente_id')->references('id')->on('diente')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedInteger('cara_dental_id');
            $table->foreign('cara_dental_id')->references('id')->on('cara_dental')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cp_dental');
    }
}
