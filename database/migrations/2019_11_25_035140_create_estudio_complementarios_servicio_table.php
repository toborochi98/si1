<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstudioComplementariosServicioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudioComplementarios_servicio', function (Blueprint $table) {
            $table->unsignedInteger('servicio_id');
            $table->foreign('servicio_id','fk_estudioComplementariosservicio_servicio')->references('id')->on('servicio')->onDelete('cascade')->onUpdate('restrict');
            
            $table->unsignedInteger('estudioComplementarios_id');
            $table->foreign('estudioComplementarios_id','fk_estudioComplementariosservicio_estudioComplementarios')->references('id')->on('estudioComplementarios')->onDelete('restrict')->onUpdate('restrict')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estudioComplementarios_servicio');
    }
}
