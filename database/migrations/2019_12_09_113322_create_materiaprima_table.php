<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMateriaprimaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materiaprima', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',64);
            $table->unsignedInteger('codigo');
            $table->unsignedInteger('stock');
            $table->string('descripcion',64)->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materiaprima');
    }
}
