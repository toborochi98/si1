$(function(){
	function EdicionOdontograma(){
		var self = this;

		self.tratamientosPosibles = ko.observableArray([
			{ "id":"02.01", "nombre":"Obturacion c/ amalgama: cavidad simple - solo cara", "aplicaCara":true, "aplicaDiente":false },
			{ "id":"02.05", "nombre":"Obt.con Ionómero cav.simple - sin cara y sin diente", "aplicaCara":false, "aplicaDiente":false },
			{ "id":"10.16.03", "nombre":"Más de 3 cm.- de diámetro", "aplicaCara":true, "aplicaDiente":true },
			{ "id":"10.17", "nombre":"Ext. de tumores en tejidos bla", "aplicaCara":true, "aplicaDiente":true },
			{ "id":"10.17.01", "nombre":"Hasta 1 cm. de diámetro", "aplicaCara":true, "aplicaDiente":true },
			{ "id":"10.17.02", "nombre":"De 1 a 3 cm. de diámetro", "aplicaCara":true, "aplicaDiente":true },
			{ "id":"10.17.03", "nombre":"De mas de 3 cm. de diámetro", "aplicaCara":true, "aplicaDiente":true },
			{ "id":"10.18", "nombre":"Ext. alv. ext. y restos rad.", "aplicaCara":true, "aplicaDiente":true }

		]);
		self.tratamientoSeleccionado = ko.observable(null);
		self.tratamientosAplicados = ko.observableArray([]);

		self.quitarTratamiento = function(tratamiento){
			self.tratamientosAplicados.remove(tratamiento);			
			$("#odontograma").odontograma('removeTratamiento', tratamiento);
		}

		self.guardar = function(){
			var tratamientos = $("#odontograma").odontograma('getTratamientosAplicados');
			console.log(JSON.stringify(tratamientos));

			var datatab = JSON.stringify({data: tratamientos})

			$.ajaxSetup({
				headers: {
				  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			  });

			//POST
			$.ajax({
				type : "post",
				dataType: 'json',
				url: 'http://127.0.0.1:8000/admin/odontogramaTest',     
				data: {json:datatab}, 
				success : function(response)
				{
				  alert(response);
				}
			  });
			
		}

		self.tratamientoSeleccionado.subscribe(function(tratamiento){
			$("#odontograma").odontograma('setTratamiento', tratamiento);
		});
	}


	var vm = new EdicionOdontograma();	
	ko.applyBindings(vm);

	//Cargo los tratamientos
	$.getJSON('tratamientos.js', function(d){
		for (var i = d.length - 1; i >= 0; i--) {
			var tratamiento = d[i];
			vm.tratamientosPosibles.push(tratamiento);
		};		
	});

	$("#odontograma").odontograma().bind('tratamientoAplicado.odontograma', function(evt, tratamiento){
		vm.tratamientosAplicados.push(tratamiento);
	});
})