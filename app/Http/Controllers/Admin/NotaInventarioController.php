<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Pagos\NotaInventario;
use App\Models\Admin\MateriaPrima;
use App\Http\Controllers\Controller;

class NotaInventarioController extends Controller
{
    public function index()
    {
        $datas = NotaInventario::with('materiasprimas:id,nombre')->orderBy('id')->get();
        return view('admin.nota-inventario.index', compact('datas'));
    }

    public function create()
    {
        $materiasprimas = MateriaPrima::orderBy('id')->pluck('nombre','id')->toArray();
        return view('admin.nota-inventario.crear',compact('materiasprimas'));
    }

  
    public function store(Request $request)
    {
       // $notainventario= new NotaInventario;
      
        $notainventario = NotaInventario::create($request->all());
        $notainventario->materiasprimas()->sync($request->materiaprimas_id);
        return redirect('admin/nota-inventario')->with('mensaje', 'Nota Inventario creado con éxito');
    }

   
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $materias = MateriaPrima::orderBy('id')->pluck('nombre', 'id')->toArray();
        $data = NotaInventario::with('materias')->findOrFail($id);
        return view('admin.nota-inventario.editar', compact('data', 'materias'));
    }

  
    public function update(Request $request, $id)
    {
        
        $notainventario= NotaInventario::findOrFail($id);
        $notainventario->update(array_filter($request->all()));
        $notainventario->materiasprimas()->sync($request->materiaprimas_id);
        return redirect('admin/nota-inventario')->with('mensaje', 'Nota actualizado con éxito');
    }

 
    public function delete(Request $request, $id)
    {
        if ($request->ajax()) {
            if (NotaInventario::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
