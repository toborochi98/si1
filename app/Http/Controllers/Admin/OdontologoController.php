<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Seguridad\Odontologo;
use App\Models\Admin\Especialidad;
class OdontologoController extends Controller
{
    
    public function index()
    {
        $datas = Odontologo::with('especialidades:id,nombre')->orderBy('ci')->get();
        return view('admin.odontologo.index', compact('datas'));
    }

   
    public function create()
    {
        $especialidades = Especialidad::orderBy('id')->pluck('nombre','id')->toArray();
        return view('admin.odontologo.crear',compact('especialidades'));
    }

   
    public function store(Request $request)
    {
        $odontologo = Odontologo::create($request->all());
        $odontologo->especialidades()->sync($request->especialidad_id);
        return redirect('admin/odontologo')->with('mensaje', 'Oodontologo creado con exito');
    }
   
    public function edit($ci)
    {
        $especialidades = Especialidad::orderBy('id')->pluck('nombre', 'id')->toArray();
        $data = Odontologo::with('especialidades')->findOrFail($ci);
        return view('admin.odontologo.editar', compact('data', 'especialidades'));
    }

    public function update(Request $request, $ci)
    {
        $odontologo = Odontologo::findOrFail($ci);
        $odontologo->update(array_filter($request->all()));
        $odontologo->especialidades()->sync($request->especialidad_id);
        return redirect('admin/odontologo')->with('mensaje', 'Odontologo actualizado con exito');

        //Odontologo::findOrFail($ci)->update($request->all());
        //return redirect('admin/odontologo')->with('mensaje', 'Odontologo actualizado con exito');
    }

  
    public function delete(Request $request, $ci)
    {
        if ($request->ajax()) {
            if (Odontologo::destroy($ci)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
