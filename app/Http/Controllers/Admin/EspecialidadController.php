<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Especialidad;
use App\Models\Seguridad\Bitacora;


class EspecialidadController extends Controller
{
    public function index()
    {
        $datas = Especialidad::orderBy('id')->get();
        return view('admin.especialidad.index', compact('datas'));
    }

    public function create()
    {
        return view('admin.especialidad.crear');
    }

    public function store(Request $request)
    {
        // Ejemplo de insercion en Bitacora
        Bitacora::create([
            'usuario_id' => $request->user()->id,
            'tabla' => 'Especialidad',
            'accion' => 'Insertar',
            'fecha' => date("Y-m-d H:m:s", time())
            ]);
       
        Especialidad::create($request->all());
        return redirect('admin/especialidad')->with('mensaje', 'Especialidad creada con exito');
    }

    public function edit($id)
    {
        $data = Especialidad::findOrFail($id);
        return view('admin.especialidad.editar', compact('data'));
    }

    public function update(Request $request, $id)
    {
        // Ejemplo de insercion en Bitacora
        Bitacora::create([
            'usuario_id' => $request->user()->id,
            'tabla' => 'Especialidad',
            'accion' => 'Actualizar',
            'fecha' => date("Y-m-d H:m:s", time())
            ]);
        Especialidad::findOrFail($id)->update($request->all());
        return redirect('admin/especialidad')->with('mensaje', 'Especialidad actualizada con exito');
    }
    
       
    public function delete(Request $request, $id)
    {
        if ($request->ajax()) {
            if (Especialidad::destroy($id)) {
                // Ejemplo de insercion en Bitacora
                Bitacora::create([
                    'usuario_id' => $request->user()->id,
                    'tabla' => 'Especialidad',
                    'accion' => 'Elimina',
                    'fecha' => date("Y-m-d H:m:s", time())
                ]);
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
