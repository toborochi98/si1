<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Servicio;
use App\Models\Admin\EstudioComplementario;
use App\Models\Seguridad\Bitacora;


class ServicioController extends Controller
{
   
    public function index()
    {
        $datas = Servicio::with('estudiosComplementarios:id,nombre')->orderBy('id')->get();
        return view('admin.servicio.index', compact('datas'));
    }

    public function create()
    {
        $estudiosComplementarios = EstudioComplementario::orderBy('id')->pluck('nombre','id')->toArray();
        return view('admin.servicio.crear',compact('estudiosComplementarios'));
    }

  
    public function store(Request $request)
    {
        // Ejemplo de insercion en Bitacora
        Bitacora::create([
            'usuario_id' => $request->user()->id,
            'tabla' => 'Servicio',
            'accion' => 'Insertar',
            'fecha' => date("Y-m-d H:m:s", time())
            ]);
        $servicio = Servicio::create($request->all());
        $servicio->estudiosComplementarios()->sync($request->estudioComplementarios_id);
        return redirect('admin/servicio')->with('mensaje', 'Servicio creado con éxito');
    }

   
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $estudios = EstudioComplementario::orderBy('id')->pluck('nombre', 'id')->toArray();
        $data = Servicio::with('estudios')->findOrFail($id);
        return view('admin.servicio.editar', compact('data', 'estudios'));
    }

  
    public function update(Request $request, $id)
    {
        // Ejemplo de insercion en Bitacora
        Bitacora::create([
            'usuario_id' => $request->user()->id,
            'tabla' => 'Servicio',
            'accion' => 'Editar',
            'fecha' => date("Y-m-d H:m:s", time())
            ]);
        $servicio = Servicio::findOrFail($id);
        $servicio->update(array_filter($request->all()));
        $servicio->estudiosComplementarios()->sync($request->estudioComplementarios_id);
        return redirect('admin/servicio')->with('mensaje', 'Servicio actualizado con éxito');
    }

 
    public function delete(Request $request, $id)
    {
        if ($request->ajax()) {
            if (Servicio::destroy($id)) {
                // Ejemplo de insercion en Bitacora
        Bitacora::create([
            'usuario_id' => $request->user()->id,
            'tabla' => 'Servicio',
            'accion' => 'Eliminar',
            'fecha' => date("Y-m-d H:m:s", time())
            ]);
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
