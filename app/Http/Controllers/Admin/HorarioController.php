<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Agenda\Horario;

class HorarioController extends Controller
{
   
    public function index()
    {
        $datas = Horario::orderBy('id')->get();
        return view('admin.horario.index', compact('datas'));
    }

    public function create()
    {
        return view('admin.horario.crear');
    }

  
    public function store(Request $request)
    {
        Horario::create($request->all());
        return redirect('admin/horario')->with('mensaje', 'Horario creado con éxito');
    }

   
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Horario::findOrFail($id);
        return view('admin.horario.editar', compact('data'));
    }

  
    public function update(Request $request, $id)
    {
        Horario::findOrFail($id)->update($request->all());
        return redirect('admin/horario')->with('mensaje', 'Horario actualizado con éxito');
    }

 
    public function delete(Request $request, $id)
    {
        if ($request->ajax()) {
            if (Horario::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
