<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Seguridad\Paciente;

class PacienteController extends Controller
{
   
    public function index()
    {
        $datas = Paciente::orderBy('ci')->get();
        return view('admin.paciente.index',compact('datas'));
    }

    public function create()
    {
        return view('admin.paciente.crear');
    }

   
    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

   
    public function edit($ci)
    {
        $data = Paciente::findOrFail($ci);
        return view('admin.paciente.editar', compact('data'));
    }

  
    public function update(Request $request, $id)
    {
        Paciente::findOrFail($id)->update($request->all());
        return redirect('admin/paciente')->with('mensaje', 'Paciente actualizado con exito');
    }

    public function delete($id)
    {
        //
    }
}
