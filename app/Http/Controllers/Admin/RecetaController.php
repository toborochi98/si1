<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Receta;
use App\Models\Seguridad\Bitacora;


class RecetaController extends Controller
{
   
    public function index()
    {
        $datas = Receta::orderBy('id')->get();
        return view('admin.receta.index', compact('datas'));
    }

    public function create()
    {
        return view('admin.receta.crear');
    }

  
    public function store(Request $request)
    {
        // Ejemplo de insercion en Bitacora
        Bitacora::create([
            'usuario_id' => $request->user()->id,
            'tabla' => 'Receta',
            'accion' => 'Insertar',
            'fecha' => date("Y-m-d H:m:s", time())
            ]);
       
        Receta::create($request->all());
        return redirect('admin/receta')->with('mensaje', 'Receta creada con exito');
    }

    public function edit($id)
    {
        $data = Receta::findOrFail($id);
        return view('admin.receta.editar', compact('data'));
    }

    public function update(Request $request, $id)
    {
        // Ejemplo de insercion en Bitacora
        Bitacora::create([
            'usuario_id' => $request->user()->id,
            'tabla' => 'Receta',
            'accion' => 'Actualizar',
            'fecha' => date("Y-m-d H:m:s", time())
            ]);
        Receta::findOrFail($id)->update($request->all());
        return redirect('admin/receta')->with('mensaje', 'Receta actualizada con exito');
    }
    
       
    public function delete(Request $request, $id)
    {
        if ($request->ajax()) {
            if (Receta::destroy($id)) {
                // Ejemplo de insercion en Bitacora
                Bitacora::create([
                    'usuario_id' => $request->user()->id,
                    'tabla' => 'Receta',
                    'accion' => 'Eliminar',
                    'fecha' => date("Y-m-d H:m:s", time())
                ]);
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
