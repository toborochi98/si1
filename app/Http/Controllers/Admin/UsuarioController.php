<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Seguridad\Usuario;
use App\Models\Seguridad\Odontologo;
use App\Models\Seguridad\Paciente;
use App\Models\Seguridad\Recepcionista;
use App\Models\Admin\Rol;
use App\Models\Admin\Historial;
use App\Http\Requests\ValidacionUsuario;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UsuarioController extends Controller
{

    public function index()
    {
        $datas = Usuario::orderBy('id')->get();
        return view('admin.usuario.index', compact('datas'));
    }


    public function create()
    {
        $rols = Rol::orderBy('id')->pluck('nombre', 'id')->toArray();
        return view('admin.usuario.crear', compact('rols'));
    }


    public function store(ValidacionUsuario $request)
    {
       
       
        
            $usuario = new Usuario;
            $usuario->username = $request->username;
            $usuario->password = Hash::make($request->password);
            $usuario->save();
            //$usuario  = Usuario::create($request->all());
            //$usuario ->roles()->attach($request->rol_id);
            $usuario->roles()->attach($request->rol_id);

            if (in_array('2', $request->get('rol_id'), true)) {
                // Es Odontologo
                $odontologo = new Odontologo;
                $odontologo->ci = $request->ci;
                $odontologo->nombre = $request->nombre;
                $odontologo->apellido_paterno =  $request->apellido_paterno;
                $odontologo->apellido_materno = $request->apellido_materno;
                $odontologo->fecha_nacimiento = $request->fecha_nacimiento;
                $odontologo->domicilio  = $request->domicilio;
                $odontologo->usuario_id = $usuario->id;
                $odontologo->save();
            } else
                   if (in_array('3', $request->get('rol_id'), true)) {
                $paciente = new Paciente;
                $paciente->ci = $request->ci;
                $paciente->nombre = $request->nombre;
                $paciente->apellido_paterno = $request->apellido_paterno;
                $paciente->apellido_materno = $request->apellido_materno;
                $paciente->fecha_nacimiento  = $request->fecha_nacimiento;
                $paciente->domicilio  = $request->domicilio;
                $paciente->usuario_id = $usuario->id;
                $paciente->save();

                $historial = new Historial;
                $historial ->fecha_registro = date("Y-m-d H:m:s", time());
                $historial ->ultima_consulta = null;
                $historial ->paciente_ci = $request->ci;;
                $historial->save();

            } else
                   if (in_array('4', $request->get('rol_id'), true)) {
                $recepcionista = new Recepcionista;
                $recepcionista->ci = $request->ci;
                $recepcionista->nombre = $request->nombre;
                $recepcionista->apellido_paterno = $request->apellido_paterno;
                $recepcionista->apellido_materno = $request->apellido_materno;
                $recepcionista->fecha_nacimiento  = $request->fecha_nacimiento;
                $recepcionista->domicilio  = $request->domicilio;
                $recepcionista->usuario_id = $usuario->id;
                $recepcionista->save();
            }
         
            return redirect('admin/usuario')->with('mensaje', 'Usuario creado con exito');
        
    }


    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $rols = Rol::orderBy('id')->pluck('nombre', 'id')->toArray();
        $data = Usuario::with('roles')->findOrFail($id);
        
        $persona = Usuario::find($id)->odontologos;
        
        if($persona==null){
            $persona = Usuario::find($id)->pacientes;
        }

        if($persona==null){
            $persona = Usuario::find($id)->recepcionistas;
        }
        
        return view('admin.usuario.editar', compact('data', 'rols','persona'));
    }

    

    public function update(Request $request, $id)
    {
        $usuario = Usuario::findOrFail($id);
       
        $usuario->update(array_filter($request->all()));
        $usuario->roles()->sync($request->rol_id);
        return redirect('admin/usuario')->with('mensaje', 'Usuario actualizado con exito');
    }


    public function delete(Request $request, $id)
    {
        if ($request->ajax()) {
            $usuario = Usuario::findOrFail($id);
            $usuario->roles()->detach();
            $usuario->delete();
            return response()->json(['mensaje' => 'ok']);
        } else {
            abort(404);
        }
    }
}
