<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;

class OdontogramaController extends Controller
{
    public function index()
    {
        return view('admin.odontograma.index2');
    }

    public function store(Request $request)
    {
        $users = json_decode($request->json()->all());

        return redirect('admin')->with('mensaje', 'Guardado con exito');
    }
}
