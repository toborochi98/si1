<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Consulta;
use App\Http\Controllers\Controller;
use App\Models\Admin\Cita;
use App\Models\Admin\Historial;
use App\Models\Seguridad\Odontologo;
use App\Models\Seguridad\Paciente;
use App\Odontograma;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\Const_;

class ConsultaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarioActual = auth()->user();
        $listaOdontologos=Odontologo::all();
        return view('admin.consulta.index',compact('listaOdontologos'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      
       $citasModel=Cita::where('id','=',$request->citaSelectionId)->first();
     // return $citasModel->paciente_id;
    
       return view('admin.consulta.create',compact(['citasModel']));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
 
        $consulta=new Consulta();
        
        $consulta->motivo=$request->inputMotivo;
        $consulta->diagnostico= $request->inputDiagnostico;
        $consulta->tratamiento= $request->inputTratamiento;
        $consulta->fecha_retorno= $request->inputFecha;
        $consulta->historial_id=$request->inputHistorial;
        $consulta->cita_id= $request->inputCita;
        $consulta->save();
        $odontograma=new Odontograma();
        $odontograma->consulta_id=$consulta->id;
        $odontograma->save();
        return   redirect('admin/consulta')->with('mensaje', 'Cita Guardada con exito');
   
        return $consulta;    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function verCitas(Request $request)
    {   
    
        $odontologoSelected=Odontologo::where('ci','=',$request->odontologoSelection)->firstOrFail();
        $citasLista=Cita::where('odonto_id','=',$request->odontologoSelection)->get();
        
        return view('admin.consulta.citasSelection',compact(['odontologoSelected','citasLista']));
        
    }
    public static function getHistorial($idCita){
        $citasActual=Cita::where('id','=',$idCita)->first();
        $paciente=Paciente::where('ci','=',$citasActual->paciente_id)->first();
        $historial=Historial::where('paciente_ci','=',$paciente->ci)->first();;
        if(is_null($historial)){
           $historial=new Historial;
           $historial->fecha_registro=getdate();
           $historial->paciente_ci=$paciente->ci;
           $historial->save();
          
        }else{

        }
        return $historial->id;
    }
}
