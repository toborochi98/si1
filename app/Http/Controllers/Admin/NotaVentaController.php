<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pagos\NotaVenta;
use App\Models\Pagos\Cuota;
use App\Models\Seguridad\Bitacora;


class NotaVentaController extends Controller
{
   
    public function index()
    {
        $datas = NotaVenta::with('cuotas:id,estado, monto')->orderBy('id')->get();
        return view('admin.notaVenta.index', compact('datas'));
    }

   
    public function create()
    {
        $cuotas = Cuota::orderBy('id')->pluck('estado, monto','id')->toArray();
        return view('admin.notaVenta.crear',compact('cuotas'));
    }

   
    public function store(Request $request)
    {
        $notaVenta = NotaVenta::create($request->all());
        $notaVenta->cuotas()->sync($request->especialidad_id);
        return redirect('admin/notaVenta')->with('mensaje', 'Nota de Venta creada con exito');
    }
   
    public function edit($id)
    {
        $cuotas = Cuota::orderBy('id')->pluck('estado, monto', 'id')->toArray();
        $data = NotaVenta::with('cuotas')->findOrFail($id);
        return view('admin.notaVenta.editar', compact('data', 'cuotas'));
    }

    public function update(Request $request, $id)
    {
        $notaVenta = NotaVenta::findOrFail($id);
        $notaVenta->update(array_filter($request->all()));
        $notaVenta->cuotas()->sync($request->especialidad_id);
        return redirect('admin/notaVenta')->with('mensaje', 'NotaVenta actualizado con exito');

        //NotaVenta::findOrFail($id)->update($request->all());
        //return redirect('admin/notaVenta')->with('mensaje', 'NotaVenta actualizado con exito');
    }

  
    public function delete(Request $request, $id)
    {
        if ($request->ajax()) {
            if (NotaVenta::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}