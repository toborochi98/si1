<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Seguridad\Recepcionista;

class RecepcionistaController extends Controller
{
    public function index()
    {
        $datas = Recepcionista::orderBy('ci')->get();
        return view('admin.recepcionista.index',compact('datas'));
    }

    public function create()
    {
        return view('admin.recepcionista.crear');
    }

    public function store(Request $request)
    {
        //
    }

    public function edit($ci)
    {
        $data = Recepcionista::findOrFail($ci);
        return view('admin.recepcionista.editar', compact('data'));
    }

    public function update(Request $request, $id)
    {
        Recepcionista::findOrFail($id)->update($request->all());
        return redirect('admin/recepcionista')->with('mensaje', 'Recepcionista actualizado con exito');
    }

 
    public function delete(Request $request, $ci)
    {
        if ($request->ajax()) {
            if (Recepcionista::destroy($ci)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
