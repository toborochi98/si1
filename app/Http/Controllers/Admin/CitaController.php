<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Cita;
use App\Models\Seguridad\Bitacora;
use App\Models\Seguridad\Odontologo;
use App\Models\Seguridad\Paciente;
use App\Models\Admin\Usuario;
use App\Models\Admin\Rol;
use App\Models\Admin\MenuRol;
use Illuminate\Support\Facades\DB;

class CitaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       /**Verificamos si hay odontologos logueados */
       $usuarioActual = auth()->user();
     
       $odontologoLogueado=Odontologo::where('usuario_id','=',$usuarioActual->id)->first();
      /**si esta vacio */
    
        if(is_null($odontologoLogueado)){
            $allOdontologos=Odontologo::all();
         
        }
        else{
    
         //   $odontologoLogueado=$odontologoLogueado->toArray();
            $odontologoLogueado=collect([$odontologoLogueado]);
            $allOdontologos=Odontologo::all();
            
            $allOdontologos=$odontologoLogueado; 
        }
      //  return view('admin.cita.index',['allOdontologos'=>$allOdontologos]);
       return view('admin.cita.index',compact('allOdontologos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
          // Ejemplo de insercion en Bitacora
 
        //
        //  $insertArr = [ 'title' => $request->title,
        //                'start' => $request->start,
        //                'end' => $request->end
        //             ];
        // $event = Event::insert($insertArr);   
        // return Response::json($event);
        $events = [];
        //$citasLista = Cita::all();
            /**limitamos arreglo a citas del odontologo seleccionado */
            
        $citasLista=Cita::where('odonto_id','=',$request->odontologoSelection)->get();
        
       
   
        // if($citasLista->count()) {
        //     foreach ($citasLista as $key => $value) {
        //         $eventsCalen[] = (
        //             // $value->odonto_id,
        //             // true,
        //             // new \DateTime($value->hora_inicio),
        //             // new \DateTime($value->hora_inicio.' +1 day'),
        //             // null,
        //             // Add color and link on event
        //             [
        //                 'title' => 'Un titulo generico',
        //                 'start' => new \DateTime($value->hora_inicio),
        //                 'end' => new \DateTime($value->hora_inicio.' +1 day'),
        //                 'color' => '#f05050',
        //                 'url' => 'pass here url and any route',
        //             ]
        //         );
        //     }
        // }
        
     //   return $eventsCalen;
        $odontologo=Odontologo::where('ci','=',$request->odontologoSelection)->firstOrFail();
        $pacientes=Paciente::all();
     //   $citasLista=Cita::all();
        
        //$odontologo= $request->odontologoSelection;
    //   return view('admin.cita.time');
        return view('admin.cita.crear',compact(['odontologo','pacientes','citasLista']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        
        $odontologo=Odontologo::where('ci','=',$request->odontologoSelect)->firstOrFail();
     
        $paciente=Paciente::where('ci','=',$request->pacienteSelect)->firstOrFail();
        $odontologo=Odontologo::where('ci','=',$request->odontologoSelect)->firstOrFail();
        $cita=new Cita();
        
        $cita->paciente_id=$paciente->ci;
        $cita->odonto_id=$odontologo->ci;
        $cita->hora_inicio=$request->horaInput;
        $cita->hora_fin= \Carbon\Carbon::createFromFormat('H:i', $request->horaInput);
        $cita->hora_fin->addMinutes(30);
        $cita->noHayCitaEnEseHorario();
        $hoy =\Carbon\Carbon::now()->toDateTimeString();
        $cita->fecha=$hoy;
        $cita->estado=true;
        $cita->save();

        Bitacora::create([
            'usuario_id' => $request->user()->id,
            'tabla' => 'Citas',
            'accion' => 'Insertar',
            'fecha' => date("Y-m-d H:m:s", time())
            ]);
        return   redirect('admin/cita')->with('mensaje', 'Cita Guardada con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        /*
        $id = \Auth::user()->id;
        
        $items = DB::table('usuario_rol')
            ->select('rol_id')
            ->where('usuario_id','=', $id)
            ->first();
           
                
        $items = $items->get('rol_id');
        if($items==1){
            $datas = Cita::orderBy('id')->get();
        }else
        if($items==2)
        {
            $ci_o = DB::table('odontologo')
            ->select('ci')
            ->where('usuario_id','=', $id)
            ->first();
            dd($ci_o);
            $datas = Cita::where('odonto_id','=', $ci_o );
        }
        */
        $datas=Cita::all();
        return view('admin.listado-citas.index',compact('datas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
