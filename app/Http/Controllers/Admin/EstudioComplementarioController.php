<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\EstudioComplementario;
use App\Models\Seguridad\Bitacora;



class EstudioComplementarioController extends Controller
{
    public function index()
    {
        $datas = EstudioComplementario::orderBy('id')->get();
        return view('admin.estudiosComplementarios.index', compact('datas'));
    }

    public function create()
    {
        return view('admin.estudiosComplementarios.crear');
    }

  
    public function store(Request $request)
    {
        // Ejemplo de insercion en Bitacora
        Bitacora::create([
            'usuario_id' => $request->user()->id,
            'tabla' => 'Estudio Complementario',
            'accion' => 'Insertar',
            'fecha' => date("Y-m-d H:m:s", time())
            ]);
        EstudioComplementario::create($request->all());
        return redirect('admin/estudiosComplementarios')->with('mensaje', 'Estudio Complementario creado con éxito');
    }

   
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = EstudioComplementario::findOrFail($id);
        return view('admin.estudiosComplementarios.editar', compact('data'));
    }

  
    public function update(Request $request, $id)
    {
                // Ejemplo de insercion en Bitacora
                Bitacora::create([
                    'usuario_id' => $request->user()->id,
                    'tabla' => 'Estudio Complementario',
                    'accion' => 'Actualizar',
                    'fecha' => date("Y-m-d H:m:s", time())
                    ]);
        EstudioComplementario::findOrFail($id)->update($request->all());
        return redirect('admin/estudiosComplementarios')->with('mensaje', 'Estudio Complementario actualizado con éxito');
    }

 
    public function delete(Request $request, $id)
    {
        if ($request->ajax()) {
            if (EstudioComplementario::destroy($id)) {
                        // Ejemplo de insercion en Bitacora
        Bitacora::create([
            'usuario_id' => $request->user()->id,
            'tabla' => 'Estudio Complementario',
            'accion' => 'Eliminar',
            'fecha' => date("Y-m-d H:m:s", time())
            ]);
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
