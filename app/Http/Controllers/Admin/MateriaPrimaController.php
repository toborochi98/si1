<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\MateriaPrima;
class MateriaPrimaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = Materiaprima::orderBy('id')->get();
        return view('admin.materia-prima.index',compact('datas'));
    }


    public function create()
    {
        return view('admin.materia-prima.crear');
    }

   
    public function store(Request $request)
    {
        MateriaPrima::create($request->all());
        return redirect('admin/materia-prima')->with('mensaje', 'Materia Prima creada con éxito');
    }

    
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        $data = MateriaPrima::findOrFail($id);
        return view('admin.materia-prima.editar', compact('data'));
    }

  
    public function update(Request $request, $id)
    {
        MateriaPrima::findOrFail($id)->update($request->all());
        return redirect('admin/materia-prima')->with('mensaje', 'Material actualizado con exito');
    }

    
    public function delete(Request $request, $id)
    {
        if ($request->ajax()) {
            if (MateriaPrima::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
