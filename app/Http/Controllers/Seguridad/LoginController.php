<?php

namespace App\Http\Controllers\Seguridad;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\Seguridad\Bitacora;


class LoginController extends Controller
{

    use AuthenticatesUsers;
    protected $redirectTo = '/admin';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index()
    {
        return view('seguridad.index');
    }

    protected function authenticated(Request $request, $user)
    {
        $roles = $user->roles()->get();
        if ($roles->isNotEmpty()) {
            // Ejemplo de insercion en Bitacora
        Bitacora::create([
            'usuario_id' => $request->user()->id,
            'tabla' => 'Sesion',
            'accion' => 'Iniciar',
            'fecha' => date("Y-m-d H:m:s", time())
            ]);
            $user->setSession($roles->toArray());
        } else {
   
            $this->guard()->logout();
            $request->session()->invalidate();
            return redirect('seguridad/login')->withErrors(['error' => 'Este usuario no tiene un rol activo']);
        }
    }

    public function username()
    {
        return 'username';
    }
}
