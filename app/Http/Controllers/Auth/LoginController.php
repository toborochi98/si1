<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\Seguridad\Bitacora;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
                 // Ejemplo de insercion en Bitacora
                 Bitacora::create([
                    'usuario_id' => $request->user()->id,
                    'tabla' => 'Sesion',
                    'accion' => 'Cerrar',
                    'fecha' => date("Y-m-d H:m:s", time())
                    ]);
        $this->middleware('guest')->except('logout');
    }
}
