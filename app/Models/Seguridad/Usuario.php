<?php

namespace App\Models\Seguridad;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Admin\Rol;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use App\Models\Seguridad\Odontologo;
use App\Models\Seguridad\Paciente;
use App\Models\Seguridad\Recepcionista;

class Usuario extends Authenticatable
{
    protected $remember_token = false;
    protected $table = 'usuario';
    protected $fillable = ['username','password'];

    public function odontologos()
    {
        return $this->hasOne(Odontologo::class);
    }

    public function pacientes()
    {
        return $this->hasOne(Paciente::class);
    }

    public function recepcionistas()
    {
        return $this->hasOne(Recepcionista::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Rol::class, 'usuario_rol');
    }

    public function setSession($roles)
    {
        Session::put([
            'username' => $this->username,
            'usuario_id' => $this->id
        ]);
        if (count($roles) == 1) {
            Session::put(
                [
                    'rol_id' => $roles[0]['id'],
                    'rol_nombre' => $roles[0]['nombre'],
                ]
            );
        } else {
            Session::put('roles', $roles);
        }
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::needsRehash($password) ? Hash::make($password) : $password;
    }
}
