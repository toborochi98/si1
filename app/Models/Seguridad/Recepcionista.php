<?php

namespace App\Models\Seguridad;

use Illuminate\Database\Eloquent\Model;

class Recepcionista extends Model
{
    protected $primaryKey = 'ci';
    protected $table = 'recepcionista';
    protected $fillable = [
        'ci',
        'nombre',
        'apellido_paterno',
        'apellido_materno',
        'fecha_nacimiento',
        'domicilio',
        'correo',
        'telefono',
        'usuario_id',
    ];
}
