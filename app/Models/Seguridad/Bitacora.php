<?php

namespace App\Models\Seguridad;

use Illuminate\Database\Eloquent\Model;

class Bitacora extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'bitacora';
    protected $fillable = [
        'id',
        'usuario_id',
        'tabla',
        'accion',
        'fecha'
    ];
    public $timestamps = false;
}
