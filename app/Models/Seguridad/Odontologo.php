<?php

namespace App\Models\Seguridad;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\Especialidad;
use App\Models\Agenda\Horario;

class Odontologo extends Model
{
    protected $primaryKey = 'ci';
    protected $table = 'odontologo';
    protected $fillable = [
        'ci',
        'nombre',
        'apellido_paterno',
        'apellido_materno',
        'fecha_nacimiento',
        'domicilio',
        'correo',
        'telefono',
        'usuario_id',
    ];

    public function usuarios()
    {
        return $this->belongsTo(Usuario::class);

    }

    public function especialidades()
    {
        return $this->belongsToMany(Especialidad::class, 'especialidad_odontologo');
    }

    public function horario()
    {
        return $this->belongsToMany(Horario::class, 'horarios_odontologo');
    }

}
