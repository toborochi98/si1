<?php

namespace App\Models\Pagos;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pagos\Cuota;
use App\Models\Admin\Consulta;

class NotaVenta extends Model
{
    protected $table = "notaVenta";
    protected $fillable=
    [   'consulta_id',
        'montoTotal',
        'saldo',
        'fecha'
    ];
    protected $guarded =['id'];

    public function cuotas()
    {
        return $this->hasMany(Cuota::class, 'notaVenta_id');
    }

    public function consulta()
    {
        return $this->belongsTo(Consulta::class, 'consulta_id');
    }    
    
}
