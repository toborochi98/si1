<?php

namespace App\Models\Pagos;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pagos\NotaVenta;
use App\Models\Seguridad\Paciente;

class Cuota extends Model
{
    protected $table = "cuota";
    protected $fillable=
    [   'paciente_id',
        'notaVenta_id',
        'monto',
        'estado',
        'fecha'
    ];
    protected $guarded =['id'];

    public function notaVenta()
    {
        return $this->belongsTo(NotaVenta::class, 'notaVenta_id');
    }
    
    public function pacientes()
    {
        return $this->belongsTo(Paciente::class, 'paciente_id');
    }

    
}
