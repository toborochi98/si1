<?php

namespace App\Models\Pagos;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\MateriaPrima;

class NotaInventario extends Model
{
    protected $table = "notainventario";
    protected $fillable=
       
    [  
        'fecha',
        'nit',
        'nombre',
        'cantidad',
        'precio'
    ];
    protected $guarded =['id'];

    public function materiasprimas()
    {
        return $this->belongsToMany(MateriaPrima::class, 'nota_materiaprima', 'notainventario_id','materiaprimas_id');
    }
}
