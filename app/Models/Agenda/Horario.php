<?php

namespace App\Models\Agenda;

use Illuminate\Database\Eloquent\Model;

class Horario extends Model
{
    protected $table = 'horarios';
    protected $fillable=[
                         'dia',
                         'hora_inicio',
                         'hora_fin'   
                        ];
    protected $guarded =['id'];                    
}
