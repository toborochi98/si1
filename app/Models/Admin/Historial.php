<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Historial extends Model
{
    protected $primaryKey = 'id';
    protected $table = "historial";
    protected $fillable=['fecha_registro','ultima_consulta','paciente_ci'];
    protected $guarded =['id'];
    public $timestamps  = false;


}
