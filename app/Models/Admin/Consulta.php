<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Consulta extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'consulta';
    protected $fillable = [
        'id',
        'motivo',
        'diagnostico',
        'tratamiento',
        'fecha_retorno',
        'historial_id',
        'cita_id'
    ];
    public $timestamps = false;


}
