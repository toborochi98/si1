<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Diente extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'diente';
    protected $fillable = [
        'id',
        'nombre',
        'nro',
        'estado_actual'
    ];
    public $timestamps  = false;

}
