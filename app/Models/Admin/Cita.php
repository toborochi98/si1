<?php

namespace App\Models\Admin;;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;
use SoftDeletes;
class Cita extends Model
{
    
    //
    protected $table = 'citas';
    public function noHayCitaEnEseHorario()
    {
        $citas = Cita::all();
        foreach ($citas as $cita) {

            if ($this->tiempoDeCitaIgual($cita)) {
                abort(403, 'Su cita choca con otra');
            }
        }
        return true;
    }
    public function tiempoDeCitaIgual(Cita $cita)
    {

        // $tiempoInicio = Carbon::now();
        // $tiempoInicio->copy( Carbon::parse($cita->hora_inicio));
        // $tiempoInicio = \Carbon\Carbon::parse($cita->hora_inicio)->format('H:m');
        // $tiempoFin = \Carbon\Carbon::parse($cita->hora_fin)->format('H:m');
        $tiempoInicio = \Carbon\Carbon::parse($cita->fecha . $cita->hora_inicio);
        $tiempoFin = \Carbon\Carbon::parse($cita->fecha . $cita->hora_fin);
        $tiempoInicioInput = \Carbon\Carbon::parse($this->fecha . $this->hora_inicio);
        $tiempoFinInput = \Carbon\Carbon::parse($this->fecha . $this->hora_fin);
        // $tiempoInicioInput = \Carbon\Carbon::parse($this->hora_inicio)->format('H:m');
        // $tiempoFinInput =  \Carbon\Carbon::parse($this->hora_fin)->format('H:m');
      //  $consulta =Cita::whereBetween('reservation_from', [$from, $to])->get();
        // $hora_inicioEntrada = \Carbon\Carbon::Parse($this->hora_inicio);
        // $tiempoFinEntrada = clone $hora_inicioEntrada;
        // $tiempoFinEntrada->addMinutes(30);

        // $pruebaModelo = new Cita();
        // $pruebaModelo->hora_inicio = $hora_inicioEntrada;

        // $tiempoInicioInput = \Carbon\Carbon::createFromFormat('H:i',$this->hora_inicio);
        // $time = Carbon::now(); // Current time
        // $start = Carbon::create($time->year, $time->month, $time->day, 10, 0, 0); //set time to 10:00
        // $end = Carbon::create($time->year, $time->month, $time->day, 18, 0, 0); //set time to 18:00

        if( $tiempoInicioInput->between($tiempoInicio, $tiempoFin, true)) {
            //abort(403, 'su inicio es' . $tiempoInicio . 'su final es' . $tiempoFin . 'choca con ' . $tiempoInicioInput);
            //
            abort(403, 'Su Cita Choca con otra, por favor revise la agenda y vuelva a intentar');
        
       }else{
            
       }
       if( $tiempoFinInput->between($tiempoInicio, $tiempoFin, true)) {  
                abort(403, 'Su Cita Choca con otra, por favor revise la agenda y vuelva a intentar');
            }else{
                    
            }
        // $users = DB::table('cita')
        //         ->whereColumn([
        //             [$tiempoInicioInput, '>=',  $tiempoInicio],
        //             [$tiempoInicioInput, '<=', $tiempoFin]
        //         ])->get();
        // $consulta=Cita::whereTime($hora_inicioEntrada, '>=', \Carbon\Carbon::parse($tiempoInicio))
        //     ->whereTime($hora_inicioEntrada, '<=', \Carbon\Carbon::parse($tiempoFin));
    //     if ($users == null) {
    //         return false;
    //     }
    //     else{
    //     abort(403, 'su inicio es' . $tiempoInicio . 'su final es' . $tiempoFin . 'choca con ' . $tiempoInicioInput);
    // }



        // $hora_inicioEntrada = Carbon::parse($cita->hora_inicio);
        // $tiempoFinEntrada= $hora_inicioEntrada->addMinutes(30);
        // if($cita->whereTime($tiempoFinEntrada, '>=', \Carbon\Carbon::parse($hora_inicioThis))
        // ->whereTime($tiempoFinEntrada, '<=', \Carbon\Carbon::parse($tiempoFinThis))){
        //     return true;
        // }
    }
}
