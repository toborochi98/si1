<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class CaraDental extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'cara_dental';
    protected $fillable = [
        'id',
        'nombre'
    ];
}
