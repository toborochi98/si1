<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receta extends Model
{
    protected $table = "receta";
    protected $fillable=
    [   'descripcion',
        'consulta_id'
    ];
    protected $guarded =['id'];

    public function consulta()
    {
        return $this->belongsTo(Consulta::class);

    }
}


