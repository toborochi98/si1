<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\Servicio;

class EstudioComplementario extends Model
{
     protected $table = "estudioComplementarios";
     protected $fillable = ['nombre', 'descripcion'];
     protected $guarded = ['id'];

     public function servicios()
    {
        return $this->belongsToMany(Servicio::class, 'estudioComplementarios_servicio','estudioComplementarios_id', 'servicio_id');
    }
}
