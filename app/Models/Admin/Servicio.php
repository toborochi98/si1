<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\EstudioComplementario;

class Servicio extends Model
{
    protected $table = "servicio";
    protected $fillable=
    [   'nombre',
        'descripcion',
        'tiempoDeDuracion'
    ];
    protected $guarded =['id'];

    public function estudiosComplementarios()
    {
        return $this->belongsToMany(EstudioComplementario::class, 'estudioComplementarios_servicio', 'servicio_id','estudioComplementarios_id');
    }
}
