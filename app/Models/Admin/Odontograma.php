<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Odontogramas extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'odontogramas';
    protected $fillable = [
        'id',
        'consulta_id',
    ];

}
