<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pagos\NotaInventario;

class MateriaPrima extends Model
{
    protected $table="materiaprima";
    protected $fillable=['nombre','codigo','stock','descripcion'];
    protected $guarded =['id'];

    public function notainventarios()
    {
        return $this->belongsToMany(NotaInventario::class, 'nota_materiaprima', 'materiaprimas_id','notainventario_id');
    }
}