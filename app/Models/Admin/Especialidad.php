<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Especialidad extends Model
{
    protected $table = "especialidad";
    protected $fillable=['nombre','descripcion'];
    protected $guarded =['id'];
    
}
