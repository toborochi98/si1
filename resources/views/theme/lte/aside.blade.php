<aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
    
          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="{{asset("assets/$theme/si_user_icon_white.png")}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>{{session()->get('rol_nombre')}}</p>
            </div>
          </div>
    
          <!-- Sidebar Menu -->
          <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Menú Principal</li>
              @foreach ($menusComposer as $key => $item)
                  @if ($item["menu_id"] != 0)
                      @break
                  @endif
                  @include("theme.$theme.menu-item", ["item" => $item])
              @endforeach
          </ul>
          <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>