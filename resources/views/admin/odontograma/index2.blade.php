@extends("theme.$theme.layout")
@section("titulo")
Odontograma
@endsection

@section("styles")
<link href="{{asset("assets/js/jquery-nestable/jquery.nestable.css")}}" rel="stylesheet" type="text/css" />
@endsection

@section("scriptsPlugins")
<script src="{{asset("assets/js/jquery-nestable/jquery.nestable.js")}}" type="text/javascript"></script>
@endsection
  
@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/menu/index.js")}}" type="text/javascript"></script>
<script src="{{asset ("assets/$theme/bower_components/jquery-ui/jquery-ui.min.js")}}"></script>
@endsection

@section('contenido')



<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title"></h3>
    
<div class="container">
    
   </div>
 

  <link rel="stylesheet" href="{{asset ("assets/$theme/odontogramaBuch/public/css/style.css")}}"> 
  <link rel="stylesheet" href="{{asset ("assets/$theme/odontogramaBuch/public/css/jquery-ui-1.8.17.custom.css")}}">
  <link rel="stylesheet" href="{{asset ("assets/$theme/odontogramaBuch/public/css/jquery.svg.css")}}">
  <link rel="stylesheet" href="{{asset ("assets/$theme/odontogramaBuch/public/css/odontograma.css")}}">
  <!-- end CSS-->

  <!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->

  <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
  <script src="{{asset ("assets/$theme/odontogramaBuch/public/js/modernizr-2.0.6.min.js")}}"></script>

 
  <div id="container">
   
    <div id="main" role="main">      
        
        <div id="tratamiento">
            <h5>Tratamientos</h5>
            <form action="{{route('guardar_odontograma')}}" id="form-general" class="d-inline form-eliminar" method="POST" autocomplete="off">
            <div>
              <button type="submit" data-bind="click: guardar" class="btn btn-success">Guardar</button>
            </div>
          </form>  
            <br>
            <select  class="form-control"
              data-bind=" options: tratamientosPosibles, 
                          value: tratamientoSeleccionado, 
                          optionsText: function(item){ return item.nombre; },
                          optionsCaption: 'Seleccionar'">
            </select>
            <ul class="list-group"  data-bind="foreach: tratamientosAplicados">
              <li class="list-group-item">
                <span  data-bind="text: diente.id"></span><span data-bind="text: cara"></span>
                <span  data-bind="text: tratamiento.nombre"></span>
                <button data-bind="click: $parent.quitarTratamiento" href="#" type="button" class="btn btn-primary btn-sm">Eliminar</button>
              </li>
            </ul>
            
          </div>
      <div id="odontograma-wrapper">
        <h5>Odontograma</h5>
        <div id="odontograma"></div>
      </div>    

    </div>
    <footer>
      
    </footer>
  </div> <!--! end of #container -->  

  <!-- scripts concatenated and minified via ant build script-->
  <script defer src="{{asset ("assets/$theme/odontogramaBuch/public/js/jquery-1.7.1.min.js")}}"></script>
  <script defer src="{{asset ("assets/$theme/odontogramaBuch/public/js/knockout-2.0.0.js")}}"></script>
  <script defer src="{{asset ("assets/$theme/odontogramaBuch/public/js/jquery.svg.min.js")}}"></script>  
  <script defer src="{{asset ("assets/$theme/odontogramaBuch/public/js/jquery.svggraph.min.js")}}"></script>  
  <script defer src="{{asset ("assets/$theme/odontogramaBuch/public/js/odontograma.js")}}"></script>
  <script defer src="{{asset ("assets/$theme/odontogramaBuch/nuevo.js")}}"></script>
  <!-- end scripts-->

  <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
  <!--[if lt IE 7 ]>
    <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
    <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
  <![endif]-->
  



    </div>
</div>
@endsection
