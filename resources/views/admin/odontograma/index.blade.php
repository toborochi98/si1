@extends("theme.$theme.layout")
@section("titulo")
Odontograma
@endsection

@section("styles")
<link href="{{asset("assets/js/jquery-nestable/jquery.nestable.css")}}" rel="stylesheet" type="text/css" />
@endsection

@section("scriptsPlugins")
<script src="{{asset("assets/js/jquery-nestable/jquery.nestable.js")}}" type="text/javascript"></script>
@endsection
  
@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/menu/index.js")}}" type="text/javascript"></script>
<script src="{{asset ("assets/$theme/bower_components/jquery-ui/jquery-ui.min.js")}}"></script>
@endsection

@section('contenido')

<style>
        *{
	margin: 0;
	padding: 0;
}

body{
	background-color: #ccffe6;
	font-family: "Verdana";
}
.nros{
	width: 100%;
	text-align: center;
	max-width: 990px;
	margin: auto;
	display: grid;
	grid-template-columns:repeat(16,1fr);
	background-color: #ccffe6;
}

.contenedorCaras{
	width: 100%;
	max-width: 60px;
	border: 1px solid black;
	margin: auto;
	line-height: 50px;
}
.contenedorCaras > img{
	height: 25px;
	width: 20px;
	display: block;
	background: solid white;
	margin: auto;
	cursor: pointer;
/*	box-shadow: 2px 2px 10px rgba(0,0,0,.25);
	border: 5px solid black;*/
}

.CXBlanco{
	height: 50px;
	width: 50px;
}

.odont {
	width: 100%;
	max-width: 990px;
	border: 5px solid black;
	margin: auto;
	background-color: #e6e6e6;
}

.contenedorCaras{/*contenedor de caras*/
	display: grid;
	grid-template-columns: 1fr 1fr 1fr;
	grid-template-rows: 1fr 1fr 1fr; 
}

.odont{/*contenedor de dientes*/
	display: grid;
	grid-template-columns:repeat(16,1fr);
	grid-template-rows:  repeat(2,1fr);
}

.contenedorCaras .CSBlanco{
	grid-row-start: 1;
	grid-column: 2/3;
}

.contenedorCaras .CDBlanco{
	grid-row-start: 2;
	grid-column: 3/3;
}
.contenedorCaras .CXBlanco{
	grid-row-start: 2;
	grid-column: 2/3;
}
.contenedorCaras .CZBlanco{
	grid-row-start: 2;
	grid-column: 1/2;
}
.contenedorCaras .CIBlanco{
	grid-row-start: 3;
	grid-column: 2/3;
}


div#overlay{
  position: fixed;
  display: none;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0,0,0,0.5);
  z-index: 2;
}

.formularioOdot{
	width: 500px;
	height: 450px;
	background-color: #e6e6e6;
	border: 3px solid black;
	position: absolute;
  	top: 38%;
  	left: 50%;
  	margin-top: -170px;
   	margin-left: -250px;
   	border-radius: 6px;
}

#title{
	text-align: center;
	padding: 13px;
}

.cap{
	margin-left: 30px;
	padding-bottom: 10px;
	font-size: 20px;
}

.selection{
	width: 95%;
	height: 40px;
	margin-left: 10px;
	margin-bottom: 20px;
	padding-bottom: 10px;
	padding-top: 10px;
	font-size: 15px;
	border: 1px solid #00e699;
	background-color: #ccffee; 
	border-radius: 6px;
}

.boton{
  height: 40px;
  padding: 10px 32px;
  text-align: center;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  -webkit-transition-duration: 0.4s; /* Safari */
  transition-duration: 0.4s;
  cursor: pointer;
  background-color: #ccffee; 
  color: black; 
  border: 2px solid #00e699;
  margin-left: 80px;
  border-radius: 6px;
}

.boton:hover{
  background-color: #00e699;
  color: white;
  border-radius: 6px;
}


</style>


<script>
        
function action(imId){
	window.alert("FUNCIONA PERROS");
}
function on(imId) {
  document.getElementById("overlay").style.display = "block";
  let input = document.getElementById("idDiente");
  console.log("input", input);
  
  input.value = imId;
  
}

function off() {
  document.getElementById("overlay").style.display = "none";
}
function setNewImage(imId){

	switch (imId){
    	 
		  case '18Z':
		  case '17Z': 
		  case '16Z': 
		  case '15Z':
		  case '14Z':
		  case '12Z':
		  case '13Z':
		  case '11Z':

		  case '28Z':
		  case '27Z': 
		  case '26Z': 
		  case '25Z':
		  case '24Z':
		  case '23Z':
		  case '22Z':
		  case '21Z':

		  case '38Z':
		  case '37Z': 
		  case '36Z': 
		  case '35Z':
		  case '34Z':
		  case '33Z':
		  case '32Z':
		  case '31Z':

		  case '48Z':
		  case '47Z': 
		  case '46Z': 
		  case '45Z':
		  case '44Z':
		  case '43Z':
		  case '42Z':
		  case '41Z':
		  		document.getElementById(imId).src = "../assets/lte/odontograma/caras2/CC PNG/CZ.png";
    	  break;
    	  case '18X':
    	  case '17X':
    	  case '16X':
    	  case '15X':
		  case '14X':
		  case '13X':
		  case '12X':
		  case '11X':

		  case '28X':
		  case '27X': 
		  case '26X': 
		  case '25X':
		  case '24X':
		  case '23X':
		  case '22X':
		  case '21X':

		  case '38X':
		  case '37X': 
		  case '36X': 
		  case '35X':
		  case '34X':
		  case '33X':
		  case '32X':
		  case '31X':

		  case '48X':
		  case '47X': 
		  case '46X': 
		  case '45X':
		  case '44X':
		  case '43X':
		  case '42X':
		  case '41X':
    	  		document.getElementById(imId).src = "assets/odorecurso/odot/css/caras2/CC PNG/CX.png";
    	  break;
    	  case '18D':
    	  case '17D':
    	  case '16D':
    	  case '15D':
		  case '14D':
		  case '13D':
		  case '12D':
		  case '11D':

		  case '28D':
		  case '27D': 
		  case '26D': 
		  case '25D':
		  case '24D':
		  case '23D':
		  case '22D':
		  case '21D':

		  case '38D':
		  case '37D': 
		  case '36D': 
		  case '35D':
		  case '34D':
		  case '33D':
		  case '32D':
		  case '31D':

		  case '48D':
		  case '47D': 
		  case '46D': 
		  case '45D':
		  case '44D':
		  case '43D':
		  case '42D':
		  case '41D':
				document.getElementById(imId).src = "assets/odorecurso/odot/css/caras2/CC PNG/CD.png";
    	  break;
		  case '18I':
		  case '17I':
		  case '16I':
		  case '15I':
		  case '14I':
		  case '13I':
		  case '12I':
		  case '11I':

		  case '28I':
		  case '27I': 
		  case '26I': 
		  case '25I':
		  case '24I':
		  case '23I':
		  case '22I':
		  case '21I':

		  case '38I':
		  case '37I': 
		  case '36I': 
		  case '35I':
		  case '34I':
		  case '33I':
		  case '32I':
		  case '31I':

		  case '48I':
		  case '47I': 
		  case '46I': 
		  case '45I':
		  case '44I':
		  case '43I':
		  case '42I':
		  case '41I':
				document.getElementById(imId).src = "assets/odorecurso/odot/css/caras2/CC PNG/CI.png";
    	  break;
    	  case '18S':
    	  case '17S':
    	  case '16S':
    	  case '15S':
		  case '14S':
		  case '13S':
		  case '12S':
		  case '11S':

		  case '28S':
		  case '27S': 
		  case '26S': 
		  case '25S':
		  case '24S':
		  case '23S':
		  case '22S':
		  case '21S':

		  case '38S':
		  case '37S': 
		  case '36S': 
		  case '35S':
		  case '34S':
		  case '33S':
		  case '32S':
		  case '31S':

		  case '48S':
		  case '47S': 
		  case '46S': 
		  case '45S':
		  case '44S':
		  case '43S':
		  case '42S':
		  case '41S':
				document.getElementById(imId).src = "assets/odorecurso/odot/css/caras2/CC PNG/CS.png";
		  break;
	}	

};

function setOldImage(imId){

		switch (imId){
		  case '18Z':
		  case '17Z': 
		  case '16Z': 
		  case '15Z':
		  case '14Z':
		  case '13Z':
		  case '12Z':
		  case '11Z':

		  case '28Z':
		  case '27Z': 
		  case '26Z': 
		  case '25Z':
		  case '24Z':
		  case '23Z':
		  case '22Z':
		  case '21Z':

		  case '38Z':
		  case '37Z': 
		  case '36Z': 
		  case '35Z':
		  case '34Z':
		  case '33Z':
		  case '32Z':
		  case '31Z':

		  case '48Z':
		  case '47Z': 
		  case '46Z': 
		  case '45Z':
		  case '44Z':
		  case '43Z':
		  case '42Z':
		  case '41Z':
		  		document.getElementById(imId).src = "assets/odorecurso/odot/css/caras2/B PNG/CZ.png";
    	  break;
    	  case '18X':
    	  case '17X':
    	  case '16X':
    	  case '15X':
		  case '14X':
		  case '13X':
		  case '12X':
		  case '11X':

		  case '28X':
		  case '27X': 
		  case '26X': 
		  case '25X':
		  case '24X':
		  case '23X':
		  case '22X':
		  case '21X':

		  case '38X':
		  case '37X': 
		  case '36X': 
		  case '35X':
		  case '34X':
		  case '33X':
		  case '32X':
		  case '31X':

		  case '48X':
		  case '47X': 
		  case '46X': 
		  case '45X':
		  case '44X':
		  case '43X':
		  case '42X':
		  case '41X':
    	  		document.getElementById(imId).src = "assets/odorecurso/odot/css/caras2/B PNG/CX.png";
    	  break;
    	  case '18D':
    	  case '17D':
    	  case '16D':
    	  case '15D':
		  case '14D':
		  case '13D':
		  case '12D':
		  case '11D':

		  case '28D':
		  case '27D': 
		  case '26D': 
		  case '25D':
		  case '24D':
		  case '23D':
		  case '22D':
		  case '21D':

		  case '38D':
		  case '37D': 
		  case '36D': 
		  case '35D':
		  case '34D':
		  case '33D':
		  case '32D':
		  case '31D':

		  case '48D':
		  case '47D': 
		  case '46D': 
		  case '45D':
		  case '44D':
		  case '43D':
		  case '42D':
		  case '41D':
				document.getElementById(imId).src = "assets/odorecurso/odot/css/caras2/B PNG/CD.png";
    	  break;
    	  case '18S':
    	  case '17S':
    	  case '16S':
		  case '15S':
		  case '14S':
		  case '13S':
		  case '12S':
		  case '11S':

		  case '28S':
		  case '27S': 
		  case '26S': 
		  case '25S':
		  case '24S':
		  case '23S':
		  case '22S':
		  case '21S':

		  case '38S':
		  case '37S': 
		  case '36S': 
		  case '35S':
		  case '34S':
		  case '33S':
		  case '32S':
		  case '31S':

		  case '48S':
		  case '47S': 
		  case '46S': 
		  case '45S':
		  case '44S':
		  case '43S':
		  case '42S':
		  case '41S':
				document.getElementById(imId).src = "assets/odorecurso/odot/css/caras2/B PNG/CS.png";
    	  break;
    	  case '18I':
    	  case '17I':
    	  case '16I':
    	  case '15I':
		  case '14I':
		  case '13I':
		  case '12I':
		  case '11I':

		  case '28I':
		  case '27I': 
		  case '26I': 
		  case '25I':
		  case '24I':
		  case '23I':
		  case '22I':
		  case '21I':

		  case '38I':
		  case '37I': 
		  case '36I': 
		  case '35I':
		  case '34I':
		  case '33I':
		  case '32I':
		  case '31I':

		  case '48I':
		  case '47I': 
		  case '46I': 
		  case '45I':
		  case '44I':
		  case '43I':
		  case '42I':
		  case '41I':
				document.getElementById(imId).src = "assets/odorecurso/odot/css/caras2/B PNG/CI.png";
    	  break;

	}

};
</script>


<br><br>
<br>
    <h1 class="text-center">ODONTOGRAMA</h1>
<div class="nros">
<h2>18</h2>
<h2>17</h2>
<h2>16</h2>
<h2>15</h2>
<h2>14</h2>
<h2>13</h2>
<h2>12</h2>
<h2>11</h2>

<h2>21</h2>
<h2>22</h2>
<h2>23</h2>
<h2>24</h2>
<h2>25</h2>
<h2>26</h2>
<h2>27</h2>
<h2>28</h2>
</div>
<div class="odont">

     <div class= "contenedorCaras">
<!--  En el ID esta yendo el numero del diente y la cara dental -->                     
<img id="18S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)" class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="18D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)" class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="18X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)" class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="18Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)" class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="18I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)" class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">

      </div>

      <div class= "contenedorCaras">

<img id="17S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)" class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="17D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)" class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="17X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)" class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="17Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)" class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="17I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)" class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
      </div>

      <div class= "contenedorCaras">

<img id="16S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="16D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="16X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="16Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="16I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
      </div>

      <div class= "contenedorCaras">

<img id="15S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="15D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="15X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="15Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="15I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
      </div>
<div class= "contenedorCaras">

<img id="14S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="14D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="14X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="14Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="14I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="13S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="13D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="13X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="13Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="13I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="12S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="12D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="12X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="12Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)" class="CZBlanco"  src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="12I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="11S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="11D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="11X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="11Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="11I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="21S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="21D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="21X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="21Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="21I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="22S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="22D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="22X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="22Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="22I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="23S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="23D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="23X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="23Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="23I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="24S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="24D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="24X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="24Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="24I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="25S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="25D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="25X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="25Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="25I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="26S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="26D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="26X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="26Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="26I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="27S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="27D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="27X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="27Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="27I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="28S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="28D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="28X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="28Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="28I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="48S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="48D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="48X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="48Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="48I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="47S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="47D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="47X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="47Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="47I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="46S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco"  src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="46D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco"  src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="46X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco"  src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="46Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco"  src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="46I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"   class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="45S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="45D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="45X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="45Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="45I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="44S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="44D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="44X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="44Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="44I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="43S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="43D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="43X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="43Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="43I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="42S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="42D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="42X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="42Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="42I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="41S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="41D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="41X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="41Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="41I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="31S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="31D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="31X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="31Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="31I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="32S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="32D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="32X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="32Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="32I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="33S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="33D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="33X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="33Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="33I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="34S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="34D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="34X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="34Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="34I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="35S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="35D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="35X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="35Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="35I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="36S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="36D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="36X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="36Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="36I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="37S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="37D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="37X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="37Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="37I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
 <div class= "contenedorCaras">

<img id="38S" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CSBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CS.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="38D" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CDBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CD.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="38X" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CXBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CX.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="38Z" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CZBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CZ.png")}}" alt="NO SALIO BIEN TODAVIA :(">
<img id="38I" onclick="on(this.id)" onmouseover="setNewImage(this.id)" onmouseout="setOldImage(this.id)"  class="CIBlanco" src="{{asset("assets/$theme/odontograma/caras2/B PNG/CI.png")}}" alt="NO SALIO BIEN TODAVIA :(">
            
 </div>
</div>
<div class="nros">
<h2>48</h2>
<h2>47</h2>
<h2>46</h2>
<h2>45</h2>
<h2>44</h2>
<h2>43</h2>
<h2>42</h2>
<h2>41</h2>

<h2>31</h2>
<h2>32</h2>
<h2>33</h2>
<h2>34</h2>
<h2>35</h2>
<h2>36</h2>
<h2>37</h2>
<h2>38</h2>
</div>

@endsection
