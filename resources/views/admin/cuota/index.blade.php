@extends("theme.$theme.layout")
@section('titulo')
Cuotas 
@endsection

@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
<div class="row">
    <div class="col-lg-12">
        @include('includes.mensaje')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">CUOTAS</h3>
            </div>
            <div class="box-body">
                <table class="table table-striped table-bordered table-hover" id="tabla-data">
                    <thead>
                        <tr>
                            <th>Monto</th>
                            <th>Estado</th>
                            <th>Fecha</th>
                            <th class="width70"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datas as $data)
                        <tr>
                            <td>{{$data->monto}}</td>
                            <td>{{$data->estado}}</td>
                            <td>{{$data->fecha}}</td>
                            <td>
                                <form action="{{route('eliminar_especialidad', ['id' => $data->id])}}" class="d-inline form-eliminar" method="POST">
                                    @csrf @method("delete")
                                    <button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Cuota ya pagada">
                                        <i class="fa fa-fw fa-times-circle text-danger"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>

                <div class="box-tools pull-right">
                    <a href="{{route('notaVenta')}}" class="btn btn-block btn-success btn-sm">
                        <i class="fa fa-fw fa-plus-circle"></i> Ver la nota de Venta
                    </a>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection