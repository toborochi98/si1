<div class="form-group">
    <label for="dia" class="col-lg-3 control-label requerido">Dia</label>
    <div class="col-lg-8">
    <input type="text" name="dia" id="dia" class="form-control" value="{{old('dia', $data->dia ?? '')}}" required/>
    </div>
</div>

<div class="form-group">
    <label for="nombre" class="col-lg-3 control-label">Hora de Inicio</label>
    <div class="col-lg-8">
        <div class="input-group date">
        <input type="time" id="hora_inicio" name="hora_inicio"
            min="00:00" max="23:00" class="form-control" value="{{old('dia', $data->hora_inicio ?? '')}}" required>
          </div>
    </div>
</div>

<div class="form-group">
    <label for="nombre" class="col-lg-3 control-label">Hora Final</label>
    <div class="col-lg-8">
        <div class="input-group date">
        <input type="time" id="hora_fin" name="hora_fin"
            min="00:00" max="23:00" class="form-control" value="{{old('dia', $data->hora_fin ?? '')}}" required>
          </div>
    </div>
</div>