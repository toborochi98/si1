
    
    <div class="form-group">
        <label for="nombre" class="col-lg-3 control-label requerido">Nombre</label>
        <div class="col-lg-8">
        <input type="text" name="nombre" id="nombre" class="form-control" value="{{old('nombre', $data->nombre ?? '')}}" required/>
        </div>
    </div>
    <div class="form-group">
        <label for="codigo" class="col-lg-3 control-label requerido">Codigo</label>
        <div class="col-lg-8">
        <input type="text" name="codigo" id="codigo" class="form-control" value="{{old('codigo', $data->codigo ?? '')}}" required/>
        </div>
    </div>
    
    <div class="form-group">
        <label for="stock" class="col-lg-3 control-label requerido">Stock</label>
        <div class="col-lg-8">
        <input type="text" name="stock" id="stock" class="form-control" value="{{old('stock', $data->stock ?? '')}}" required/>
        </div>
    </div>

    <div class="form-group">
        <label for="descripcion" class="col-lg-3 control-label">Descripcion</label>
        <div class="col-lg-8">
        <input type="text" name="descripcion" id="descripcion" class="form-control" value="{{old('descripcion', $data->descripcion ?? '')}}"/>
        </div>
    </div>
  
    
    
  