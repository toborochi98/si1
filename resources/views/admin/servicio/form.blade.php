<div class="form-group">
    <label for="nombre" class="col-lg-3 control-label requerido">Nombre</label>
    <div class="col-lg-8">
    <input type="text" name="nombre" id="nombre" class="form-control" value="{{old('nombre', $data->nombre ?? '')}}" required/>
    </div>
</div>

<div class="form-group">
    <label for="descripcion" class="col-lg-3 control-label requerido">Descripcion</label>
    <div class="col-lg-8">
    <input type="text" name="descripcion" id="descripcion" class="form-control" value="{{old('descripcion', $data->descripcion ?? '')}}" required/>
    </div>
</div>

<div class="form-group">
    <label for="tiempoDeDuracion" class="col-lg-3 control-label requerido">Tiempo de Duración (min)</label>
    <div class="col-lg-8">
    <input type="number" name="tiempoDeDuracion" id="tiempoDeDuracion" class="form-control" value="{{old('tiempoDeDuracion', $data->tiempoDeDuracion ?? '')}}" required/>
    </div>
</div>

<div class="form-group">
    <label for="estudioComplementarios_id" class="col-lg-3 control-label requerido">Estudios Complementarios</label>
    <div class="col-lg-8">
        <select name="estudioComplementarios_id[]" id="estudioComplementarios_id" class="form-control" multiple>
            @foreach($estudiosComplementarios as $id => $nombre)
                <option
                value="{{$id}}"
                {{is_array(old('estudioComplementarios_id')) ? (in_array($id, old('estudioComplementarios_id')) ? 'selected' : '')  : (isset($data) ? ($data->EstudiosComplementarios->firstWhere('id', $id) ? 'selected' : '') : '')}}
                >
                {{$nombre}}
                </option>
            @endforeach
        </select>
    </div>
</div>