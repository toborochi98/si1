@extends("theme.$theme.layout")
@section('titulo')
Pacientes
@endsection

@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
<br><h3><center><strong>MODIFICAR DATOS DEL DIENTE</strong></center></h3><hr/>
<div class="container-fluid row mt-4 ml-1 flex-center">
    <div class="col">
        @if(!empty($datosdiente))
        @foreach($datosdiente as $diente)
        <form enctype="multipart/form-data" method="POST" action="">
        {!! csrf_field() !!}
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>CODIGO:</label>
                    <input type="text" class="form-control" id="txtCodigo" name="txtCodigo" size="40" value="" required>
                </div>
                <div class="form-group col-md-6">
                    <label>NUMERO:</label>
                    <input type="text" class="form-control" id="txtNumero" name="txtNumero" size="40" value="" required>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label>DESCRIPCION:</label>
                    <input type="text" class="form-control" id="txtDescripcion" name="txtDescripcion" size="40" value="" required>
                </div>
            </div>
            @endforeach
            @endif

            <br><br>
                                                    
            <div class="row flex-center">
                <div class="col-14 flex-center">
                    <input class="btn btn-primary" type="submit" value="GUARDAR" id="btnGuardar" name="btnGuardar">
                </div>
                <div>&nbsp;</div>
                <div class="col-14 flex-center">
                    <input class="btn btn-secondary" type="button" value="CANCELAR" id="btnCancelar" name="btnCancelar" onclick="">
                </div>
            </div>
            </form>
            
        </div>
    </div>
    
<link href="{{asset('css/style-basic.css')}}" rel="stylesheet" type="text/css"/>
@endsection