@extends("theme.$theme.layout")
@section('titulo')
Recepcionistas
@endsection

@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
<div class="row">
    <div class="col-lg-12">
        @include('includes.mensaje')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Recepcionistas</h3>
                <div class="box-tools pull-right">
                    <a href="{{route('crear_recepcionista')}}" class="btn btn-block btn-success btn-sm">
                        <i class=""></i> Nuevo registro
                    </a>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped table-bordered table-hover" id="tabla-data">
                    <thead>
                        <tr>
                            <th>CI</th>
                            <th>Nombre</th>
                            <th>Apellido Paterno</th>
                            <th>Apellido Materno</th>
                            <th>Domicilio</th>
                            <th>Telefono</th>
                            <th class="width70"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datas as $data)
                        <tr>
                            <td>{{$data->ci}}</td>
                            <td>{{$data->nombre}}</td>
                            <td>{{$data->apellido_paterno}}</td>
                            <td>{{$data->apellido_materno}}</td>
                            <td>{{$data->domicilio}}</td>
                            <td>{{$data->telefono}}</td>
                            <td>
                                <a href="{{route('editar_recepcionista', ['ci' => $data->ci])}}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
                                    <i class="fa fa-fw fa-pencil"></i>
                                </a>
                                <form action="{{route('eliminar_recepcionista', ['ci' => $data->ci])}}" class="d-inline form-eliminar" method="POST">
                                    @csrf @method("delete")
                                    <button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Eliminar este registro">
                                        <i class="fa fa-fw fa-trash text-danger"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection