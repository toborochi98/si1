@extends("theme.$theme.layout")
@section('titulo')
   Seleccion│Consultas 
@endsection
@section('contenido')
@include('includes.mensaje')
<form action="{{route('verCitasConsulta')}}" id="form-general"  
class="form-horizontal" method="GET" autocomplete="off">

<div class="box box-default" style="">
     
      @include('admin.consulta.formSelect')
      
   
</div>
</form>
@endsection