@extends("theme.$theme.layout")
@section('titulo')
   Seleccion│Citas Pendientes 
@endsection
@section('contenido')
@include('includes.mensaje')
<form action="{{route('crear_consulta')}}" id="form-general"  
class="form-horizontal" method="GET" autocomplete="off">

<div class="box box-default" style="">
     
<div class="box-body">
         
    <label for="inputGroupSelect04">Seleccione Cita Pendiente a atender </label>
        <p> Citas de {{$odontologoSelected->nombre}}</p>
        <select class="form-control" id="inputGroupSelect04" 
        name="citaSelectionId" aria-label="Example select with button addon">
   
          @foreach ($citasLista as $citas)
              <option value="{{$citas->id}}" >FECHA:{{$citas->fecha}} HORA DE INICIO :{{$citas->hora_incio}}
                    HORA DE FIN :{{$citas->hora_fin}}</option>
          @endforeach
          
        </select>
        <div class="box-body">

            <button type="submit" class="btn btn-success"> Crear Consulta De esta Cita </button>
            <a href="{{route('consulta_index')}}" class="btn btn-success" > volver</a>
        </div>
        <div class="box-body">
            </div>
        </form>
@endsection