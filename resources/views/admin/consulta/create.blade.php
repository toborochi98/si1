@extends("theme.$theme.layout")
@section('titulo')
    Consulta
@endsection
@section('contenido')
@include('includes.mensaje')
<form action="{{route('guardar_consulta')}}" id="form-general"  
class="form-horizontal" method="POST" autocomplete="off">
@csrf
<div class="box box-default">
    <div class="text-center">
            <div class="box-header">
               <label for="">Creacion de Consulta</label>
                </div>
 
    <div class="box-body">

    <div class="input-group">
        <label for="nombre" class="col-lg-3 control-label requerido">Motivo</label>
        <div class="col-lg-8">
        <input id="inputMotivo" type="text" class="form-control" name="inputMotivo">
    </div>
        <label for="nombre" class="col-lg-3 control-label requerido">diagnostico</label>
    <div class="col-lg-8">
            <textarea class="form-control" aria-label="With textarea" name="inputDiagnostico"></textarea>
        
    </div>
  
        <label for="nombre" class="col-lg-3 control-label requerido">Tratamiento</label>
        <div class="col-lg-8">
 
        <input id="tratamiento" type="text" class="form-control" name="inputTratamiento">
    </div>
    <label for="nombre" class="col-lg-3 control-label requerido">Fecha de Regreso</label>
    <div class="col-lg-8">
    
    <input  id="tratamiento" type="date" value="" class="form-control" name="inputFecha">
    </div>
    <label for="nombre" class="col-lg-3 control-label requerido">idCita</label>
    <div class="col-lg-8">
    
    <input  id="tratamiento" type="text" value="{{$citasModel->id}}" class="form-control" name="inputCita">
    </div>

    </div>

    <label for="nombre" class="col-lg-3 control-label requerido">id Historial</label>
    <div class="col-lg-8">
    
    <input  id="tratamiento" type="text" value=" {{ App\Http\Controllers\Admin\ConsultaController::getHistorial($citasModel->id) }}" class="form-control" name="inputHistorial">
    </div>

    </div>

    @include('includes.boton-form-crear')  
    <a href="{{route('odontograma')}}" class="btn btn-default">ir a odontograma</a>
    </div>        
       </div>
    
</div>
</div>
</form>
@endsection