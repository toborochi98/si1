@extends("theme.$theme.layout")
@section('titulo')
Listado
@endsection
@section('styles')
<link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection" />
<link href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection" />
    
@endsection
@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>

<script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
<script 
    type="text/javascript" 
    src="https://code.jquery.com/jquery-3.3.1.js">
</script>

<script 
    type="text/javascript" 
    src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js">
</script>

<script 
    type="text/javascript" 
    src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js">
</script>

<script 
    type="text/javascript" 
    src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js">
</script>    

<script 
    type="text/javascript" 
    src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js">
</script>

<script 
    type="text/javascript" 
    src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js">
</script>

<script 
    type="text/javascript" 
    src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js">
</script>    
 <script 
 type="text/javascript" 
 src="  https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js">
</script>
<script 
    type="text/javascript" 
    src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js">
</script>
  

<script>
    $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>


@endsection

@section('contenido')
<div class="row">
    <div class="col-lg-12">
        @include('includes.mensaje')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Listado</h3>
                <div class="box-tools pull-right">
                   
                </div>
            </div>
            <div class="box-body">
                <table id="example" class="table table-striped table-bordered table-hover" id="tabla-data">
                        
                    <thead>
                        <tr>
                            <th>Estado</th>
                            <th>Fecha</th>
                            <th>Hora Inicio</th>
                            <th>Hora Fin</th>
                            <th>Odontologo</th>
                            <th>Paciente</th>
                            <th class="width70"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datas as $data)
                        <tr>
                            <td>{{$data->estado}}</td>
                            <td>{{$data->fecha}}</td>
                            <td>{{$data->hora_inicio}}</td>
                            <td>{{$data->hora_fin}}</td>
                            <td>{{$data->odonto_id}}</td>
                            <td>{{$data->paciente_id}}</td>
                            <td>
                                <a href="{{route('editar_especialidad', ['id' => $data->id])}}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
                                    <i class="fa fa-fw fa-eye"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection