<div class="form-group">
    <label for="nombre" class="col-lg-3 control-label requerido">Username</label>
    <div class="col-lg-8">
        <input type="text" name="username" id="username" class="form-control" value="{{old('username', $data->username ?? '')}}" required/>
    </div>
</div>
<!--
<div class="form-group">
    <label for="email" class="col-lg-3 control-label {{!isset($data) ? 'requerido' : ''}}">E-Mail</label>
    <div class="col-lg-8">
        <input type="email" name="email" id="email" class="form-control" value="{{old('email', $data->email ?? '')}}" required/>
    </div>
</div>-->
<div class="form-group">
    <label for="password" class="col-lg-3 control-label {{!isset($data) ? 'requerido' : ''}}">Contraseña</label>
    <div class="col-lg-8">
        <input type="password" name="password" id="password" class="form-control" value="" {{!isset($data) ? 'required' : ''}} minlength="5"/>
    </div>
</div>
<div class="form-group">
    <label for="re_password" class="col-lg-3 control-label {{!isset($data) ? 'requerido' : ''}}">Repita Contraseña</label>
    <div class="col-lg-8">
        <input type="password" name="re_password" id="re_password" class="form-control" value="" {{!isset($data) ? 'required' : ''}} minlength="5"/>
    </div>
</div>
<div class="form-group">
    <label for="rol_id" class="col-lg-3 control-label requerido">Rol</label>
    <div class="col-lg-8">
        <select name="rol_id[]" id="rol_id" class="form-control"  required>
            <option value="">Seleccione el rol</option>
            @foreach($rols as $id => $nombre)
                <option
                value="{{$id}}"
                {{is_array(old('rol_id')) ? (in_array($id, old('rol_id')) ? 'selected' : '')  : (isset($data) ? ($data->roles->firstWhere('id', $id) ? 'selected' : '') : '')}}
                >
                {{$nombre}}
                </option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group">
    <label for="nombre" class="col-lg-3 control-label">CI</label>
    <div class="col-lg-8">
        <input type="text" name="ci" id="ci" class="form-control" value="{{old('ci', $persona->ci ?? '')}}" />
    </div>
</div>
<div class="form-group">
    <label for="nombre" class="col-lg-3 control-label">Nombre</label>
    <div class="col-lg-8">
        <input type="text" name="nombre" id="nombre" class="form-control" value="{{old('nombre', $persona->nombre ?? '')}}" />
    </div>
</div>
<div class="form-group">
    <label for="apellido_paterno" class="col-lg-3 control-label">Apellido Paterno</label>
    <div class="col-lg-8">
        <input type="text" name="apellido_paterno" id="apellido_paterno" class="form-control" value="{{old('apellido_paterno', $persona->apellido_paterno ?? '')}}" />
    </div>
</div>
<div class="form-group">
    <label for="apellido_materno" class="col-lg-3 control-label">Apellido Materno</label>
    <div class="col-lg-8">
        <input type="text" name="apellido_materno" id="apellido_materno" class="form-control" value="{{old('apellido_materno', $persona->apellido_materno ?? '')}}" />
    </div>
</div>

<div class="form-group">
    <label for="nombre" class="col-lg-3 control-label">Fecha Nacimiento</label>
    <div class="col-lg-8">
        <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" data-date-format='yyyy-mm-dd' class="form-control pull-right" name="fecha_nacimiento" id="datepicker" value="{{old('fecha_nacimiento', $persona->fecha_nacimiento ?? '')}}">
          </div>
    </div>
</div>
<!--
<div class="form-group">
    <label for="nombre" class="col-lg-3 control-label">Nacimiento</label>
    <div class="col-lg-8">
        <input type="date" name="nacimiento" id="nacimiento" class="form-control" value="" />
    </div>
</div>-->
<div class="form-group">
    <label for="domicilio" class="col-lg-3 control-label">Domicilio</label>
    <div class="col-lg-8">
        <input type="text" name="domicilio" id="domicilio" class="form-control" value="{{old('domicilio', $persona->domicilio ?? '')}}" />
    </div>
</div>
<div class="form-group">
    <label for="correo" class="col-lg-3 control-label">Correo</label>
    <div class="col-lg-8">
        <input type="text" name="correo" id="correo" class="form-control" value="{{old('correo', $persona->correo ?? '')}}" />
    </div>
</div>
<div class="form-group">
    <label for="telefono" class="col-lg-3 control-label">Telefono</label>
    <div class="col-lg-8">
        <input type="text" name="telefono" id="telefono" class="form-control" value="{{old('telefono', $persona->telefono ?? '')}}" />
    </div>
</div>