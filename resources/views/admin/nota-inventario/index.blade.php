@extends("theme.$theme.layout")
@section('titulo')
Nota Inventario
@endsection

@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
<div class="row">
    <div class="col-lg-12">
        @include('includes.mensaje')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Nota Inventario</h3>
                <div class="box-tools pull-right">
                    <a href="{{route('crear_notainventario')}}" class="btn btn-block btn-success btn-sm">
                    <i class="fa fa-fw fa-plus-circle"></i> Nueva Nota
                    </a>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped table-bordered table-hover" id="tabla-data">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Nit</th>
                            <th>Nombre de Empresa</th>
                            <th>Cantidad</th>
                            <th>Precio</th>
                            <th>Materia Prima</th>
                            <th class="width70"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datas as $data)
                        <tr>
                            <td>{{$data->fecha}}</td>
                            <td>{{$data->nit}}</td>
                            <td>{{$data->nombre}}</td>
                            <td>{{$data->cantidad}}</td>
                            <td>{{$data->precio}}</td>
                            <td>
                                @foreach ($data->materiasprimas as $materiaprima)
                                    {{$loop->last ? $materiaprima->nombre : $materiaprima->nombre . ', '}}
                                @endforeach
                            </td>
                            <td>
                
                                <form action="{{route('eliminar_notainventario', ['id' => $data->id])}}" class="d-inline form-eliminar" method="POST">
                                    @csrf @method("delete")
                                    <button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Eliminar este registro">
                                        <i class="fa fa-fw fa-trash text-danger"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection