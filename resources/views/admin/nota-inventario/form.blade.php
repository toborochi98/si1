<div class="form-group">
        <label for="fecha" class="col-lg-3 control-label">Fecha</label>
        <div class="col-lg-8">
            <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" data-date-format='yyyy-mm-dd' class="form-control pull-right" name="fecha" id="datepicker" value="{{old('fecha', $persona->fecha ?? '')}}">
              </div>
        </div>
    </div>


<div class="form-group">
    <label for="nit" class="col-lg-3 control-label requerido">Nit</label>
    <div class="col-lg-8">
    <input type="text" name="nit" id="nit" class="form-control" value="{{old('nit', $data->nit ?? '')}}" required/>
    </div>
</div>


<div class="form-group">
    <label for="nombre" class="col-lg-3 control-label requerido">Nombre de Empresa</label>
    <div class="col-lg-8">
    <input type="text" name="nombre" id="nombre" class="form-control" value="{{old('nombre', $data->nombre ?? '')}}" required/>
    </div>
</div>

<div class="form-group">
    <label for="cantidad" class="col-lg-3 control-label requerido">Cantidad(Unidad)</label>
    <div class="col-lg-8">
    <input type="number" name="cantidad" id="cantidad" class="form-control" value="{{old('cantidad', $data->cantidad ?? '')}}" required/>
    </div>
</div>

<div class="form-group">
    <label for="precio" class="col-lg-3 control-label requerido">Precio</label>
    <div class="col-lg-8">
    <input type="number" name="precio" id="precio" class="form-control" value="{{old('precio', $data->precio ?? '')}}" required/>
    </div>
</div>

<div class="form-group">
    <label for="materiaprimas_id" class="col-lg-3 control-label requerido">Materia Prima</label>
    <div class="col-lg-8">
        <select name="materiaprimas_id[]" id="materiaprimas_id" class="form-control" multiple>
            @foreach($materiasprimas as $id => $nombre)
                <option
                value="{{$id}}"
                {{is_array(old('materiaprimas_id')) ? (in_array($id, old('materiaprimas_id')) ? 'selected' : '')  : (isset($data) ? ($data->Materiasprimas->firstWhere('id', $id) ? 'selected' : '') : '')}}
                >
                {{$nombre}}
                </option>
            @endforeach
        </select>
    </div>
</div>