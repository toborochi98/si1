@extends("theme.$theme.layout")
@section('titulo')
  Citas
@endsection
@section('scripts')
    <script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
    <script src="{{asset("plugins/timepicker/bootstrap-timepicker.min.js")}}"></script>
    <link href="https://cdn.jsdelivr.net/bootstrap.timepicker/0.2.6/css/bootstrap-timepicker.min.css" rel="stylesheet" />
      @endsection

@section('contenido')

@include('includes.mensaje')
<form action="{{route('cita.create')}}" id="form-general"  
class="form-horizontal" method="GET" autocomplete="off">

<div class="box box-default" style="">
  
      @include('admin.cita.formSelect')
    
        
   
</div>
</form>
  
@endsection