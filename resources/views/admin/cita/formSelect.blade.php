<div class="text-center">

  <div class="box box-primary">
       <img src="{{asset("assets/images/dentist-icon-png-10.jpg")}}" 
      style=" margin-top: 20px ;height: 100px ; width: 100px ; background-color: #EFEFEF ;margin: 20;" class="rounded mx-auto d-block" alt="">
     
    <div class="box-body">
   
      <label for="inputGroupSelect04">Seleccione odontologo</label>
  
          <select class="form-control" id="inputGroupSelect04" 
          name="odontologoSelection" aria-label="Example select with button addon">
            {{-- <option selected>Choose...</option>
            <option value="1">One</option>
            <option value="2">Two</option>
            <option value="3">Three</option> --}}
          
            <option selected>Odontologo...</option>        
            @foreach ($allOdontologos as $odontologo)
                <option value="{{$odontologo->ci}}" >Dr. {{$odontologo->nombre}} {{$odontologo->apellido_paterno}}
                    {{$odontologo->apellido_materno}}</option>
            @endforeach
            
          </select>
          {{-- <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="button">Button</button>
          </div> --}}
          <div class="box-body">
            <button type="submit" class="btn btn-success">crear una cita</button>

          </div>
        </div>
    </div>
</div>