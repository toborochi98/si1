@extends("theme.$theme.layout")
@section('titulo')
  Citas
@endsection
@section('styles')
 <!-- Tell the browser to be responsive to screen width -->
 <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 <!-- Bootstrap 3.3.7 -->
 <link rel="stylesheet" href="{{asset ("assets/$theme/bower_components/bootstrap/dist/css/bootstrap.min.css")}}">
 <!-- Font Awesome -->
 <link rel="stylesheet" href="{{asset ("assets/$theme/bower_components/font-awesome/css/font-awesome.min.css")}}">
 <!-- Ionicons -->
 <link rel="stylesheet" href="{{asset ("assets/$theme/bower_components/Ionicons/css/ionicons.min.css")}}">
 <!-- fullCalendar -->
 <link rel="stylesheet" href="{{asset ("assets/$theme/bower_components/fullcalendar/dist/fullcalendar.min.css")}}">
 <link rel="stylesheet" href="{{asset ("assets/$theme/bower_components/fullcalendar/dist/fullcalendar.print.min.css")}}" media="print">
 <!-- Theme style -->
 <link rel="stylesheet" href="{{asset ("assets/$theme/dist/css/AdminLTE.min.css")}}">
 <!-- AdminLTE Skins. Choose a skin from the css/skins
      folder instead of downloading all of them to reduce the load. -->
 <link rel="stylesheet" href="{{asset ("assets/$theme/dist/css/skins/_all-skins.min.css")}}">
@endsection
@section('styles')
      <!-- fullCalendar -->
      <link rel="stylesheet" href="{{asset ("assets/$theme/bower_components/fullcalendar/dist/fullcalendar.min.css")}}">
      <link rel="stylesheet" href="{{asset ("assets/$theme/bower_components/fullcalendar/dist/fullcalendar.print.min.css")}}" media="print">
      <!-- Theme style -->
      <link rel="stylesheet" href="{{asset ("assets/$theme/dist/css/AdminLTE.min.css")}}">
  @endsection
@section('contenido')

        <div class="row">        

                <div class="col-lg-12">
                    @include('includes.form-error')
                    @include('includes.mensaje')
                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h3 class="box-title">Reservar</h3>
                            <div class="box-tools pull-right">
                                <a href="{{route('cita.index')}}" class="btn btn-block btn-info btn-sm">
                                    <i class="fa fa-fw fa-reply-all"></i> Volver
                                </a>
                            </div>
                        </div>
                                <form action="{{route('cita.store')}}" id="form-general" class="form-horizontal" method="POST" autocomplete="off">
                                    @csrf
                                  
                                    <div class="box-body">
                                            <div class="box-header with-border">
                                                    @include('admin.cita.horario')  
                                                    <div class="form-group">
                                                            <label for="nombre" class="col-lg-3 control-label requerido">Odontologo</label>
                                                            <div class="col-lg-8">
                                                                
                                                                    <label for="inputGroupSelect04">Odontologo Seleccionado</label>
                                                                    <select class="form-control" id="inputGroupSelect03" 
                                                                    name="odontologoSelect" aria-label="Example select with button addon" required>
                                                                      {{-- <option selected>Choose...</option>
                                                                      <option value="1">One</option>
                                                                      <option value="2">Two</option>
                                                                      <option value="3">Three</option> --}}
                                                                    
                                                                      <option selected  value="{{$odontologo->ci}}" >Dr. {{$odontologo->nombre}} 
                                                                        {{$odontologo->apellido_paterno}} {{$odontologo->apellido_materno}}</option>
                                                                     
                                                                      
                                                                    </select>
                                                            {{-- <input type="text" name="odontologoSelected" id="nombre" class="form-control" value="{{$odontologo->nombre}}
                                                            {{$odontologo->apellido_paterno}}
                                                            {{$odontologo->apellido_materno}}" disabled="" required/> --}}
                                                            </div>
                                                        </div>
                                                       
                                                        <div class="form-group">
                                                              
                                                            <label for="descripcion" class="col-lg-3 control-label">Paciente</label>
                                                            <div class="col-lg-8">
                                                                    <div class="box box-primary">
                                                                            <div class="box-body">
                                                                          
                                                                              <label for="inputGroupSelect04">Seleccione Paciente</label>
                                                                                  <select class="form-control" id="inputGroupSelect03" 
                                                                                  name="pacienteSelect" aria-label="Example select with button addon">
                                                                                    {{-- <option selected>Choose...</option>
                                                                                    <option value="1">One</option>
                                                                                    <option value="2">Two</option>
                                                                                    <option value="3">Three</option> --}}
                                                                                  
                                                                                    <option selected>Paciente...</option>
                                                                                    @foreach ($pacientes as $paciente)
                                                                                        <option value="{{$paciente->ci}}" >{{$paciente->nombre}}</option>
                                                                                    @endforeach
                                                                                    
                                                                                  </select>
                                                                        
                                                                                  
                                                                                </div>
                                                                            </div></div> 
                                                        </div>
                                                                                <!-- time Picker -->
                                                <div class="bootstrap-timepicker">
                                                        <div class="form-group">
                                                        <label>Hora:</label>
                                        
                                                        <div class="input-group">
                                                            <input id="timepick1" type="text" class="form-control timepicker" name="horaInput">
                                        
                                                            <div class="input-group-addon">
                                                            <i class="fa fa-clock-o"></i>
                                                            </div>
                                                        </div>
                                                        <!-- /.input group -->
                                                        </div>
                                                        <!-- /.form group -->
                                                
                                                </div>
                                    </div>          
                                    </div>
                                    <div class="box-footer">
                                            <div class="col-lg-3"></div>
                                            <div class="col-lg-6">
                                                @include('includes.boton-form-crear')
                                            </div>
                                </div>
                                
                                
                                </form>
                                
                                
                        
            
            
                        </div>
                    </div>
                
            </div>
           
            {{-- <link href="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" />
           
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
     
            <script src="https://cdn.jsdelivr.net/bootstrap.timepicker/0.2.6/js/bootstrap-timepicker.min.js"></script>
            
             --}}
           
            
            {{-- <script type="text/javascript">
              $('#timepicker1').timepicker({
                showInputs: false
              });
            </script> --}}
    
    
    
     
@endsection
@section('scriptsPlugins')
      <!-- iCheck 1.0.1 -->
      <script src="{{asset("assets/$theme/plugins/iCheck/icheck.min.js")}}"></script>

      <!-- jQuery 3 -->
      <script src="{{asset ("assets/$theme/bower_components/jquery/dist/jquery.min.js")}}"></script>
      <!-- Bootstrap 3.3.7 -->
      <script src="{{asset ("assets/$theme/bower_components/bootstrap/dist/js/bootstrap.min.js")}}"></script>
      <!-- jQuery UI 1.11.4 -->
      <script src="{{asset ("assets/$theme/bower_components/jquery-ui/jquery-ui.min.js")}}"></script>
      <!-- Slimscroll -->
      <script src="{{asset ("assets/$theme/bower_components/jquery-slimscroll/jquery.slimscroll.min.js")}}"></script>
      <!-- FastClick -->
      <script src="{{asset ("assets/$theme/bower_components/fastclick/lib/fastclick.js")}}"></script>
      <!-- AdminLTE App -->
      <script src="{{asset ("assets/$theme/dist/js/adminlte.min.js")}}"></script>
      <!-- AdminLTE for demo purposes -->
      <script src="{{asset ("assets/$theme/dist/js/demo.js")}}"></script>
      <!-- fullCalendar -->
      <script src="{{asset ("assets/$theme/bower_components/moment/moment.js")}}"></script>
      <script src="{{asset ("assets/$theme/bower_components/fullcalendar/dist/fullcalendar.min.js")}}"></script>
     <!-- bootstrap time picker -->
    <script src="{{asset ("assets/$theme/plugins/timepicker/bootstrap-timepicker.min.js")}}"></script>

@endsection
@section('scripts')

    <!-- Page specific script -->
      <script>
        
        $(function () {
                      function getRandomColor() {
              var letters = '0123456789ABCDEF';
              var color = '#';
              for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
              }
              return color;
            }
          /* initialize the external events
           -----------------------------------------------------------------*/
          function init_events(ele) {
            ele.each(function () {
      
              // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
              // it doesn't need to have a start or end
              var eventObject = {
                title: $.trim($(this).text()) // use the element's text as the event title
              }
      
              // store the Event Object in the DOM element so we can get to it later
              $(this).data('eventObject', eventObject)
      
              // make the event draggable using jQuery UI
              $(this).draggable({
                zIndex        : 1070,
                revert        : true, // will cause the event to go back to its
                revertDuration: 0  //  original position after the drag
              })
      
            })
          }
      
          init_events($('#external-events div.external-event'))
      
          /* initialize the calendar
           -----------------------------------------------------------------*/
          //Date for the calendar events (dummy data)
          var date = new Date()
          var d    = date.getDate(),
              m    = date.getMonth(),
              y    = date.getFullYear()
          $('#calendar').fullCalendar({
            slotDuration: '00:10:00',
            header    : {
              left  : 'prev,next today',
              center: 'title',
             // right : 'month,agendaWeek,agendaDay'
             right : 'month,agendaWeek,agendaDay'
            },
            buttonText: {
              today: 'today',
              month: 'month',
              week : 'week',
              day  : 'day'
            }, defaultView: 'agendaWeek',
            //Random default events
             events :[      @foreach($citasLista as $cita)
            {
              
                id: '{{ $cita->id }}',
                title : '{{ $pacienteaa=\App\Models\Seguridad\Paciente::where('ci','=',$cita->paciente_id)->firstOrFail()->nombre. ' Ficha:' . $cita->id }}',
                start : '{{ $cita->fecha .'T'. $cita->hora_inicio }}',
                
                   @if ($cita->hora_fin)
                       end: '{{ $cita->fecha .'T'. $cita->hora_fin }}' ,
                @endif
                color: '#0c9c78'
            },
            @endforeach],
            // events    : [
              
            //   {
            //     title          : 'Extraccion Molar',
            //     start          : new Date(y, m, 1),
            //     backgroundColor: '#f56954', //red
            //     borderColor    : '#f56954' //red
            //   },
            //   {
            //     title          : 'Curacion',
            //     start          : new Date(y, m, d - 5),
            //     end            : new Date(y, m, d - 2),
            //     backgroundColor: '#f39c12', //yellow
            //     borderColor    : '#f39c12' //yellow
            //   },
            //   {
            //     title          : 'Revision',
            //     start          : new Date(y, m, d, 10, 30),
            //     allDay         : false,
            //     backgroundColor: '#0073b7', //Blue
            //     borderColor    : '#0073b7' //Blue
            //   },
            //   {
            //     title          : 'Limpieza',
            //     start          : new Date(y, m, d, 12, 0),
            //     end            : new Date(y, m, d, 14, 0),
            //     allDay         : false,
            //     backgroundColor: '#00c0ef', //Info (aqua)
            //     borderColor    : '#00c0ef' //Info (aqua)
            //   },
            //   {
            //     title          : 'Endodoncia',
            //     start          : new Date(y, m, d + 1, 19, 0),
            //     end            : new Date(y, m, d + 1, 22, 30),
            //     allDay         : false,
            //     backgroundColor: '#00a65a', //Success (green)
            //     borderColor    : '#00a65a' //Success (green)
            //   },
            //   {
            //     title          : 'Click for Google',
            //     start          : new Date(y, m, 28),
            //     end            : new Date(y, m, 29),
            //     url            : 'http://google.com/',
            //     backgroundColor: '#3c8dbc', //Primary (light-blue)
            //     borderColor    : '#3c8dbc' //Primary (light-blue)
            //   }
            // ],´
           
            editable  : false,
            droppable : false, // this allows things to be dropped onto the calendar !!!
            drop      : function (date, allDay) { // this function is called when something is dropped
      
              // retrieve the dropped element's stored Event Object
              var originalEventObject = $(this).data('eventObject')
      
              // we need to copy it, so that multiple events don't have a reference to the same object
              var copiedEventObject = $.extend({}, originalEventObject)
      
              // assign it the date that was reported
              copiedEventObject.start           = date
              copiedEventObject.allDay          = allDay
              copiedEventObject.backgroundColor = $(this).css('background-color')
              copiedEventObject.borderColor     = $(this).css('border-color')
      
              // render the event on the calendar
              // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
              $('#calendar').fullCalendar('renderEvent', copiedEventObject, true)
      
              // is the "remove after drop" checkbox checked?
              if ($('#drop-remove').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                $(this).remove()
              }
      
            }
          })
      
          /* ADDING EVENTS */
          var currColor = '#3c8dbc' //Red by default
          //Color chooser button
          var colorChooser = $('#color-chooser-btn')
          $('#color-chooser > li > a').click(function (e) {
            e.preventDefault()
            //Save color
            currColor = $(this).css('color')
            //Add color effect to button
            $('#add-new-event').css({ 'background-color': currColor, 'border-color': currColor })
          })
          $('#add-new-event').click(function (e) {
            e.preventDefault()
            //Get value and make sure it is not null
            var val = $('#new-event').val()
            if (val.length == 0) {
              return
            }
      
            //Create events
            var event = $('<div />')
            event.css({
              'background-color': currColor,
              'border-color'    : currColor,
              'color'           : '#fff'
            }).addClass('external-event')
            event.html(val)
            $('#external-events').prepend(event)
      
            //Add draggable funtionality
            init_events(event)
      
            //Remove event from text input
            $('#new-event').val('')
          })
        })
        // $('#calendar').fullCalendar('destroy');
        // $('#calendar').fullCalendar('option','slotDuration','00:70:00');
      
      </script>
      

    {{-- <script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
    <script src="{{asset("plugins/timepicker/bootstrap-timepicker.min.js")}}"></script>
    
<link href="https://cdn.jsdelivr.net/bootstrap.timepicker/0.2.6/css/bootstrap-timepicker.min.css" rel="stylesheet" /> --}}
<script>
        $(function () {
          $('#timepick1').timepicker({
            
                
                showSeconds: false,
                showMeridian: false,
                
      showInputs: false
    })
          //Timepicker
        
        })
      </script>
       <script type="text/javascript">
        $('timepicker1').timepicker({
          showInputs: false
        });
      </script>
     
@endsection