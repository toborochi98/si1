<div class="form-group">
    <label for="ci" class="col-lg-3 control-label">CI</label>
    <div class="col-lg-8">
    <input type="text" name="ci" id="ci" class="form-control" value="{{old('ci', $data->ci ?? '')}}" required/>
    </div>
</div>

<div class="form-group">
    <label for="nombre" class="col-lg-3 control-label requerido">Nombre</label>
    <div class="col-lg-8">
    <input type="text" name="nombre" id="nombre" class="form-control" value="{{old('nombre', $data->nombre ?? '')}}" required/>
    </div>
</div>


<div class="form-group">
    <label for="apellido_paterno" class="col-lg-3 control-label requerido">Apellido Paterno</label>
    <div class="col-lg-8">
    <input type="text" name="apellido_paterno" id="apellido_paterno" class="form-control" value="{{old('apellido_paterno', $data->apellido_paterno ?? '')}}" required/>
    </div>
</div>


<div class="form-group">
    <label for="apellido_materno" class="col-lg-3 control-label requerido">Apellido Materno</label>
    <div class="col-lg-8">
    <input type="text" name="apellido_materno" id="apellido_materno" class="form-control" value="{{old('apellido_materno', $data->apellido_materno ?? '')}}" required/>
    </div>
</div>

<div class="form-group">
    <label for="fecha_nacimiento" class="col-lg-3 control-label requerido">Fecha Nacimiento</label>
    <div class="col-lg-8">
    <input type="text" name="fecha_nacimiento" id="fecha_nacimiento" class="form-control" value="{{old('fecha_nacimiento', $data->fecha_nacimiento ?? '')}}" required/>
    </div>
</div>


<div class="form-group">
    <label for="domicilio" class="col-lg-3 control-label requerido">Domicilio</label>
    <div class="col-lg-8">
    <input type="text" name="domicilio" id="domicilio" class="form-control" value="{{old('domicilio', $data->domicilio ?? '')}}" required/>
    </div>
</div>


<div class="form-group">
    <label for="correo" class="col-lg-3 control-label">Correo</label>
    <div class="col-lg-8">
    <input type="email" name="correo" id="correo" class="form-control" value="{{old('correo', $data->correo ?? '')}}"/>
    </div>
</div>


<div class="form-group">
    <label for="telefono" class="col-lg-3 control-label">Telefono</label>
    <div class="col-lg-8">
    <input type="text" name="telefono" id="telefono" class="form-control" value="{{old('telefono', $data->telefono ?? '')}}"/>
    </div>
</div>

<div class="form-group">
    <label for="especialidad_id" class="col-lg-3 control-label requerido">Especialidades</label>
    <div class="col-lg-8">
        <select name="especialidad_id[]" id="especialidad_id" class="form-control" multiple>
            <option value="">Seleccione Especialidades</option>
            @foreach($especialidades as $id => $nombre)
                <option
                value="{{$id}}"
                {{is_array(old('especialidad_id')) ? (in_array($id, old('especialidad_id')) ? 'selected' : '')  : (isset($data) ? ($data->especialidades->firstWhere('id', $id) ? 'selected' : '') : '')}}
                >
                {{$nombre}}
                </option>
            @endforeach
        </select>
    </div>
</div>